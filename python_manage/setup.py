#!/usr/bin/python3
from setuptools import setup, find_packages

# helps installing on every environment and loads dependencies
# to install nethinks_mange run: python3 setup.py install
# to develop on these program better run: python3 setup.py develop
# for more help to change this setup see: https://click.palletsprojects.com/en/7.x/setuptools/


#required packages arent loaded acutally --> ??
setup(
    name='NT_uCPEMan',
    version='0.2',
    packages=  ['nethinks_manage'], #find_packages(),
    #package_data = {'nethinks_manage': ['nethinks_manage/*']},
    #py_modules=['nethinks_manage'],
    include_package_data=True,
    install_requires=[
        'Click',
        'pyfiglet',
        'sshtunnel',
        'paramiko',
        'scp',
        'wget',
        'libvirt-python',
        'pyfastcopy',
        'ruamel.yaml',
        'ryu',
        'utils',
        'Flask',
        'requests',
        'flask_cors'
    ],
    entry_points= {
        'console_scripts': [
            'NTuCPEMan = nethinks_manage.nethinks_manage:cli',
        ],
    }
)
