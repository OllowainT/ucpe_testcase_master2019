#!/usr/bin/env python3

# This is used to tunnel a ssh-connection to the local kvm-hypervisor, wich is only
# local available

# requirements: paramiko, sshtunnel
#
#
import paramiko
import sshtunnel
import time
from threading import Thread, Event
import threading
#import daemon

__author__ = "Markus Betz"

# MANAGER_IP = "192.168.1.203"
# KVM_SERVER = "10.10.100.1"
# KVM_PORT = 22
# MANAGER_PORT = 50500

class Ssh_port_tunnel(Thread):
    def __init__(self, hypervisor_ip, hypervisor_port, manager_ip, manager_port, user, password, activate_session):
        self.hypervisor_ip = hypervisor_ip
        self.hypervisor_port = hypervisor_port
        self.manager_ip = manager_ip
        self.manager_port = manager_port
        self.user = user
        self.password = password
        self._stop = Event()

    def run(self):
        self.session_active = True
        print("\nTry to start local ssh tunnel on IP: %s and Port %5i\n" % (self.manager_ip, self.manager_port))

        #only for testing:
        # for i in range(5):
        #     time.sleep(1)

        try:
            with sshtunnel.open_tunnel(
                (self.hypervisor_ip, self.hypervisor_port),
                ssh_username= self.user,
                ssh_password=self.password,
                remote_bind_address=(self.hypervisor_ip, self.hypervisor_port),
                local_bind_address=(self.manager_ip, self.manager_port)
            ) as tunnel:
                print("\nConnection to Hypervisor on IP: %s and Port %5i\n" % (self.hypervisor_ip , self.hypervisor_port))
                client = paramiko.SSHClient()
                client.load_system_host_keys()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(self.hypervisor_ip, self.hypervisor_port, username=self.user, password=self.password)
                while not self._stop.is_set():
                    #print("tunnel session is active...listening on IP: %s and Port %5i\n" % (self.manager_ip, self.manager_port))
                    time.sleep(1)
                #client.exec_command("mkdir -p /datastore/sshtunnel")
                #client.exec_command("rm -r /datastore/sshtunnel")
        except:
            print("There was an error while trying to set up ssh tunnel to Hypervisor")



    def stop(self): #doesnt work yet
        self._stop.set()

    def __delete__(self):
        print("ssh tunnel closed and deleted")

    # def get_tunnel_status():

if __name__ == "__main__":
    kvm_connection = Ssh_port_tunnel("10.10.100.1", 22, "192.168.1.203", 50500, "root", "password1!", True)
    kvm_connection.run()
