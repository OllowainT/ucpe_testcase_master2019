#!/usr/bin/env python3

import socket

ip = "127.0.0.1"
tcp_port = 50500
buffer_size = 1024


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
    server.bind((ip, tcp_port))
    server.listen(1)

    print("\nWaiting for connection on Port %5i... \n" % tcp_port)

    conn, addr = server.accept()
    print("Connection Address: ", addr )

    while 1:
        data = conn.recv(buffer_size)
        if not data:
            break
        print("received data: ", data)
        conn.send(data)
