#!/usr/bin/env python3
import sys
import subprocess
import os
import configparser

from nethinks_manage.logger import Logger


logger = Logger(__name__)

class datastoreController(object):
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
        self.hypervisorIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        self.local_mount_path = self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH")






    def mountDatastore(self, **kwargs):

        self.ssh_key = kwargs.get("ssh_key", None)
        self.type = kwargs.get("type", None)
        self.ssh_user = kwargs.get("ssh_user", None)
        self.ssh_pass = kwargs.get("ssh_pass", None)

        #if self.type == "cli":
        # try:
        rcode = subprocess.call("mkdir %s" % (self.local_mount_path), shell=True)
        print("self.type is {0}:".format(  self.type))
        if self.ssh_key is not None:
            try:
                if self.type == "cli":
                    rcode = os.system("echo "+ self.ssh_pass  + " | sshfs" + " -o" + " allow_other,password_stdin,ServerAliveInterval=15" + " %s@"%self.ssh_user  + "%s:"%self.hypervisorIp + "/datastore" + " %s"%self.local_mount_path)#" % ( ssh_pass, ssh_user, DATASTORE_IP, DATASTORE_IMAGEPATH, LOCAL_DATASTORE_MOUNT_PATH))
                elif self.type is None:
                    rcode = os.system("echo "+ self.ssh_pass  + " | sshfs" + " -o" + " allow_other,password_stdin,ServerAliveInterval=15" + " %s@"%self.config.get("STANDARD","DATASTORE_USER")   + "%s:"%self.hypervisorIp + "/datastore" + " %s"%self.local_mount_path)#" % ( ssh_pass, ssh_user, DATASTORE_IP, DATASTORE_IMAGEPATH, LOCAL_DATASTORE_MOUNT_PATH))
                if rcode is not 0:
                    logger.info("Child was terminated by signal " + str(rcode))
                    return False
                else:
                    logger.info("Child returned " + str(rcode))
                    return True
            except OSError as e:
                logger.info("An error ocurred " + str(e))
                return False
        else:
            try:
                #rcode = os.system("echo "+ self.ssh_pass  + " | sshfs" + " -o" + " allow_other,password_stdin,ServerAliveInterval=15" + " %s@"%self.ssh_user  + "%s:"%self.hypervisorIp + "/datastore" + " %s"%self.local_mount_path)#" % ( ssh_pass, ssh_user, DATASTORE_IP, DATASTORE_IMAGEPATH, LOCAL_DATASTORE_MOUNT_PATH))
                if self.type == "cli":
                    rcode = os.system("echo "+ self.ssh_pass  + " | sshfs" + " -o" + " allow_other,password_stdin,ServerAliveInterval=15" + " %s@"%self.ssh_user  + "%s:"%self.hypervisorIp + "/datastore" + " %s"%self.local_mount_path)#" % ( ssh_pass, ssh_user, DATASTORE_IP, DATASTORE_IMAGEPATH, LOCAL_DATASTORE_MOUNT_PATH))
                elif self.type is None:
                    rcode = os.system("echo "+ self.config.get("STANDARD","DATASTORE_PASS")  + " | sshfs" + " -o" + " allow_other,password_stdin,ServerAliveInterval=15" + " %s@"%self.config.get("STANDARD","DATASTORE_USER")  + "%s:"%self.hypervisorIp + "/datastore" + " %s"%self.local_mount_path)#" % ( ssh_pass, ssh_user, DATASTORE_IP, DATASTORE_IMAGEPATH, LOCAL_DATASTORE_MOUNT_PATH))

                if rcode is not 0:
                    logger.info("Child was terminated by signal " + str(rcode))
                    return False
                else:
                    logger.info("Child returned " + str(rcode))
                    return True
            except OSError as e:
                logger.info("An error ocurred " + str(e))
                return False

    def unmount_datastore(self):
        try:
            rcode = subprocess.call("fusermount -u " + self.local_mount_path, shell=True)
            if rcode is not 0:
                logger.info("Child was terminated by signal " + str(rcode))
                logger.info("error while unmounting datastore ")
                return False
            else:
                logger.info("Child returned " + str(rcode))
                logger.info("unmounted datastore ")
                return True
        except OSError as e:
            logger.info("An error ocurred - not mounted or something else " + str(e))
            return False
