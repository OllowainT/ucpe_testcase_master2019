#!/bin/sh
echo "run setup.py in dev mode"
sleep 2
# install for production instead of develop
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
python3 "$SCRIPTPATH/setup.py" develop
echo "$SCRIPTPATH"
