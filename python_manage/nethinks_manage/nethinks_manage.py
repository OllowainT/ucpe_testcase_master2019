#!/usr/bin/env python3

from pyfiglet import Figlet
import click
import yaml
from threading import Thread
import threading
import time
import subprocess
import os
import sys
import paramiko
from scp import SCPClient
import wget
import libvirt
import configparser
import requests

#own modules
from nethinks_manage.tunnel_ssh import *
from nethinks_manage.init_xml_domain import *
from nethinks_manage.logger import Logger
from nethinks_manage.configuration.config_validator import ConfigValidator
from nethinks_manage.configuration.config_parser import ConfigParser
from nethinks_manage.configuration.helper import Helper
from nethinks_manage.infrastructure.networkbuilder import NetworkBuilder
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn
from nethinks_manage.datastore import datastoreController
from nethinks_manage.infrastructure.vmbuilder import VmBuilder

# define Logger
logger = Logger(__name__)

# always read Static config Data:
config = configparser.ConfigParser()
config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))

#########################
# wrapper for pretty cli commands
#########################

# entry point --> see setup.py
@click.group()
def cli():
    logger.info("cli started")
    f = Figlet(font='slant')
    click.echo(f.renderText('NTuCPEMan'))
    click.echo(
    """
    NT_uCPEMan is an easy cli for managing a uCPE's behaviour.
    run "nethinks_manage --help" to get useful information
    """
    )
    pass

# setup connection to Hypervisor
@cli.command()
@click.option('-p', '--password', hide_input=True, required=True, help="send password for authentication", default=config.get("STANDARD","HYPERVISOR_SSH_PW"))
def enable(password):
    """
    Enable Connection to Hypervisor
    """
    if password == config.get("STANDARD","ENABLE_PW"):
        successfully = enable_zebra_manage_kvm_interface()
        if successfully == True:
            click.echo("Connection to hypervisor established")
    else:
        click.echo("wrong password")

# disable connection to Hypervisor
@cli.command()
@click.option('-p', '--password', hide_input=True, required=True, help="send password for authentication")
def disable(password):
    """
    Disable Connection to Hypervisor
    """
    if password == config.get("STANDARD","ENABLE_PW"):
        successfully = disable_zebra_manage_kvm_interface()
        if successfully == True:
            click.echo("Stopped Connection to hypervisor")
    # needs to verify a user who is allowed to use this program
    else:
        click.echo("wrong password")


@cli.command()
# @click.option('--start',  help="start or stop direct ssh tunnel to hypervisor over specific port")
@click.option('-p', '--password', hide_input=True, required=True, help="send password for authentication")
@click.option('-c', '--command', required=True, default="status", help="what should the tunnel do?", type=click.Choice(['status', 'launch', 'destroy']))
#@click.argument('command', nargs=1, required=True)

def tunnel(password, command):
    """
    Start SSH tunnel to Hypervisor
    """
    possible_args = ['status', 'launch', 'destroy']
    #todo: check password not hard coded
    if password == ENABLE_PW:
        click.echo("password correct ")
        if command == "status":
            status_ssh_tunnel()
        elif command == "launch":
            click.echo("launch tunnel...")
            run_start_ssh_tunnel()
        elif command == "destroy":
            click.echo("destroy tunnel...")
            stop_ssh_tunnel()
        else:
            click.echo("wrong arguments, possible args are: \n")
            click.echo("\n".join(possible_args))

    else:
        click.echo("wrong password")

@cli.command()
#@click.option('-c', '--command', required=True, help="show the running-config", type=click.Choice(['config']))
def zebra(): #zebra(command):
    """
    Show Routing Engine config
    """
    show_zebra_config()

@cli.command()
@click.option('-c', '--command', required=True, help="mount/unmound datastore via ssh to monarch", type=click.Choice(['mount', 'unmount']))
@click.option('-u', '--user', required=True, help="define user to connect", default=config.get("STANDARD","DATASTORE_USER"))
@click.option('-p', '--pwd', required=True, help="define password to connect", default=config.get("STANDARD","DATASTORE_PASS"))
@click.option('-k', '--ssh-key', help="define ssh_key path", default=None)
def datastore(command, user, pwd, ssh_key):
    """
    Mount or dismount datastore
    """
    if command == "mount":
        mount_datastore(ssh_user = user, ssh_pass=pwd, ssh_key=ssh_key, type="cli")
    elif command == "unmount":
        returnvalue = unmount_datastore()
        click.echo(returnvalue)


@cli.command()
@click.option('-m', '--method', required=True, help="method to load image ", type=click.Choice(['url', 'ssh']), default="ssh")
@click.option('-u', '--user', help="ssh: define user to connect", default="root")
@click.option('-p', '--pwd', help="ssh: define password to connect", default="password1!")
@click.option('-s', '--source', help="define datasource, ssh --> without path", default="192.168.1.202")
@click.option('-pa', '--path', help="path to ssh file to transfer, without filename", default="/tmp/")
@click.option('-f', '--filename', help="filename to ssh file to transfer", default="test.txt")
#@click.option('-t', '--target', help="full target path with filename", default="/")
def transfer(method, user, pwd, source, path, filename, target):
    """
    Copy data like images to datastore
    """
    click.echo(
    """
    if you want to transfor from url with user credentials,
    please provide user and pw in following form:
    https://user:password@url
    """)
    returnvalue = load_image_to_datastore(method=method, user=user, password=pwd, source_url=source, path=path, filename=filename)
        # todo: give feedback

#for test deployments - delete if build_config works
@cli.command()
def deploy():
    """
    Start test deployment
    """
    connection = open_virsh_connection()
    deploy_vm_from_image_nolinux(conn=connection)
    close_virsh_connection(conn=connection)


@cli.command()
@click.option('-f', '--filepath', required=True, help="path to config file", default=config.get("STANDARD", "UCPE_CONFIGPATH"))
def build_config(filepath):
    """
    Import and build configuration
    """
    configure_ucpe(filepath)

##########################
#
# Build uCPE infrastructure
#
##########################

@cli.command()
def build_local_network():
    """
    Build management network
    """
    connection = open_virsh_connection(dev=True)
    deploy_local_management_network(conn=connection)
    run_hyp_cmd(command="ovs-vsctl add-br lmanagement")
    close_virsh_connection(conn=connection)

@cli.command()
def init_sfc_network():
    """
    Init and build SFC-Network-Switch
    """
    connection = open_virsh_connection()
    build_sfc_network(conn=connection)
    close_virsh_connection(conn=connection)
    # this manager is also connected to the sfcbr on management portgroup.
    # The connectivity is lost until a restart of the manager!

@cli.command()
@click.option('-n', '--name', required=True,  help="name of vnf to build")
def build_vnf(name):
    """
    Build and start a VNF
    """
    connection = open_virsh_connection()
    vmlistNames = getVMsofVNF(name, conn=connection)
    for n in vmlistNames:
        click.echo("build name is {0}".format(n))
        retCode = deploy_vm(conn=connection, vmName=n)
        while retCode == None:
            time.sleep(1)
        if retCode:
            click.echo("vm {0} runs now".format(n))
            click.echo("""############################""")
        else:
            click.echo("vm {0} is not running now".format(n))
            click.echo("""############################""")
    close_virsh_connection(conn=connection)

@cli.command()
@click.option('-n', '--name', required=True, multiple=True,  help="names of vm to build (every with extra -n option)")
def build_vms(name):
    """
    Start and build list of VMs
    """
    connection = open_virsh_connection()
    for n in name:
        click.echo("build name is {0}".format(n))
        retCode = deploy_vm(conn=connection, vmName=n)
        while retCode == None:
            time.sleep(1)
        if retCode:
            click.echo("vm {0} runs now".format(n))
            click.echo("""############################""")
        else:
            click.echo("vm {0} is not running now".format(n))
            click.echo("""############################""")
    close_virsh_connection(conn=connection)

@cli.command()
@click.option('-n', '--name', required=True,  help="name of vm to build")
def build_vm(name):
    """
    Start and build a single VM
    """
    print("build name is {0}".format(name))
    connection = open_virsh_connection()
    retCode = deploy_vm(conn=connection, vmName=name)
    if retCode:
        click.echo("vm {0} runs now".format(name))
    else:
        click.echo("vm {0} is not running now".format(name))
    close_virsh_connection(conn=connection)


#########################
# normal methods
#########################

def run_start_ssh_tunnel():

    #check if process is already running
    if os.path.isfile(config.get("STANDARD","TUNNEL_PID_FILE")):
        pidfile = open(config.get("STANDARD","TUNNEL_PID_FILE"), "r")
        pid = (pidfile.read())
        click.echo("%s already exists, please stop first with pid %s" % (config.get("STANDARD","TUNNEL_PID_FILE"), pid) )
        sys.exit()
    else:
        print("dir: ", os.getcwd())
        tunnel_proc = subprocess.Popen(["%s/nethinks_manage/tunnel_ssh.py" % (os.getcwd() )], stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)
        click.echo("started tunnel as subprocess process with pid %s" % tunnel_proc.pid)
        tunnel_pid = tunnel_proc.pid
        pidfile = open(config.get("STANDARD","TUNNEL_PID_FILE"), 'w+')
        pidfile.write(str(tunnel_pid))


def stop_ssh_tunnel():

    if os.path.exists(config.get("STANDARD","TUNNEL_PID_FILE")):
        pidfile = open(config.get("STANDARD","TUNNEL_PID_FILE"), "r")
        pid = int((pidfile.read()))
        click.echo("%s exists, try to kill with pid %i" % (config.get("STANDARD","TUNNEL_PID_FILE"), pid) )
        kill_return = os.kill(pid, 9)
        click.echo("kill return code is: %s" % (kill_return))
        os.remove(config.get("STANDARD","TUNNEL_PID_FILE"))
    else:
        click.echo("process is not running or is a zombie. if tunnel is still open try to kill with 'kill -9 pid' by hand")


#todo: get some more infos
def status_ssh_tunnel():
    if not (os.path.exists(config.get("STANDARD","TUNNEL_PID_FILE"))):
        click.echo("\nthe ssh tunnel isnt running right now, so there are no infos\n")
    else:
        pidfile = open(config.get("STANDARD","TUNNEL_PID_FILE"), "r")
        pid = int((pidfile.read()))
        click.echo("ssh tunnel is started, here are some infos:")
        click.echo("Lockfile Path: %s\nPid: %i\n" % (config.get("STANDARD","TUNNEL_PID_FILE"), pid))


def show_zebra_config():
    try:
        rcode = subprocess.call("vtysh -c 'show running-config' --echo", shell=True)
        if rcode is not 0:
            click.echo("Child was terminated by signal " + str(rcode))
            return False
        else:
            click.echo("Child returned " + str(rcode))
            return True
    except OSError as e:
        click.echo("An error ocurred " + str(e))
        return False

def enable_zebra_manage_kvm_interface():
    # todo: check if its already enabled
    try:
        rcode = subprocess.call("vtysh -c 'configure terminal\ninterface ens3f1\nno shutdown' --echo", shell=True)
        if rcode is not 0:
            click.echo("Child was terminated by signal " + str(rcode))
            return False
        else:
            click.echo("Child returned " + str(rcode))
            return True
    except OSError as e:
        click.echo("An error ocurred " + str(e))
        return False

def disable_zebra_manage_kvm_interface():
    #todo: check if its already disconnected
    try:
        rcode = subprocess.call("vtysh -c 'configure terminal\ninterface ens3f1\nshutdown' --echo", shell=True)
        if rcode is not 0:
            click.echo("Child was terminated by signal " + str(rcode))
            return False
        else:
            click.echo("Child returned " + str(rcode))
            return True
    except OSError as e:
        click.echo("An error ocurred " + str(e))
        return False


##############
# helper methods
##############

def mount_datastore(**kwargs):
    #todo: mount datastore on kvm or somewhere else to specific path on monarch
    ssh_key = kwargs.get("ssh_key", None)
    type = kwargs.get("type", None)
    ssh_user = kwargs.get("ssh_user", None)
    ssh_pass = kwargs.get("ssh_pass", None)
    ds = datastoreController()
    mount = ds.mountDatastore(ssh_key=ssh_key, type=type, ssh_user=ssh_user, ssh_pass=ssh_pass)
    if mount is True:
        click.echo("successfully mounted")
    else:
        click.echo("not mounted, see log")


def unmount_datastore():
    ds = datastoreController()
    unmount = ds.unmount_datastore()
    if unmount is True:
        click.echo("successfully unmounted")
    else:
        click.echo("not unmounted, see log")


def load_image_to_datastore(**kwargs):
    source_url = kwargs.get("source_url", None)
    user = kwargs.get("user", None)
    password = kwargs.get("password", None)
    method = kwargs.get("method", None)
    path = kwargs.get("path", None)
    filename = kwargs.get("filename", None)


    if method == "url" and (user or password is None):
        click.echo("please provide user and pw in following form: https://user:password@url")
        click.echo("try to download via url without user credentials...")
        wget.download(source_url, config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/")
    elif method == "ssh":
        click.echo("try to copy via ssh...")
        print("\nConnection to Destination on: %s \n" % (source_url))
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(source_url, 22, username=user, password=password)

        ftp_client=client.open_sftp()
        pathinfo = ftp_client.listdir(path=path)
        ftp_client.get(path + filename, config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/" + filename, callback=progress_sftp)
        ftp_client.close()
    else:
        click.echo("no method given")
        return False

def progress_sftp(transferred, toBeTransferred):
    print("Transferred: {0}\tOut of: {1}" .format(transferred, toBeTransferred))

def deploy_vm_from_image_linux(**kwargs):
    filepath = kwargs.get("filepath", None)
    name = kwargs.get("name", None)
    click.echo("get vm image and deploy with params")

    conn = libvirt.open()


def open_virsh_connection(**kwargs):
    dev = kwargs.get("dev", None) # just for development - connects to config ip
    def request_cred(credentials, user_data):
        for credential in credentials:
            if credential[0] == libvirt.VIR_CRED_AUTHNAME:
                credential[4] = config.get("STANDARD","HYPERVISOR_SSH_USER")
            elif credential[0] == libvirt.VIR_CRED_PASSPHRASE:
                credential[4] = config.get("STANDARD","HYPERVISOR_SSH_PW")
        return 0

    auth = [[libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_PASSPHRASE], request_cred, None]

    if dev is True: # open a dev connection to build infrastructure
        conn = libvirt.openAuth("qemu+ssh://%s/system"%(config.get("STANDARD","DATASTORE_IP")), auth, 0)
    else:
        conn = libvirt.openAuth("qemu+ssh://%s/system"%(config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")), auth, 0)
    if conn == None:
        print('Failed to open connection', file=sys.stderr)
        return False
        exit(1)

    click.echo("connection to qemu established...\n")
    click.echo("here are some infos:\n")
    hostname = conn.getHostname()
    print('Hostname: '+ hostname)

    return conn

def close_virsh_connection(**kwargs):
    conn = kwargs.get("conn", None)
    conn.close()
    click.echo("virsh connection closed")
    exit(0)

# only if virsh connection is up
def deploy_vm_from_image_nolinux(**kwargs):
    conn = kwargs.get("conn", None)
    filepath = kwargs.get("filepath", None)
    name = kwargs.get("name", None)
    click.echo("get vm image and deploy with params")
    nodeinfo = conn.getInfo()
    print('Model: '+str(nodeinfo[0]))
    print('Memory size: '+str(nodeinfo[1])+'MB')
    print('Number of CPUs: '+str(nodeinfo[2]))
    print('MHz of CPUs: '+str(nodeinfo[3]))
    print('Number of NUMA nodes: '+str(nodeinfo[4]))
    print('Number of CPU sockets: '+str(nodeinfo[5]))
    print('Number of CPU cores per socket: '+str(nodeinfo[6]))
    print('Number of CPU threads per core: '+str(nodeinfo[7]))
    newVNF = XmlDescriptor(conn)
    newVNF.init_vm()


def vnf_directory_new(**kwargs):
    click.echo("try to mkdir in datastore dir")


def configure_ucpe(filepath):
    cv = ConfigValidator(filepath)
    configval = cv.validate_config()
    if configval:
        logger.info("config is valid")
        # validation successfull go on here
        cp = ConfigParser(filepath, config.get("STANDARD","SFCBR0_PATH"), config.get("STANDARD","VMXML_PATH"))

    else:
        logger.error("no valid config")


#just for internat development to build the local manage bridge
def deploy_local_management_network(**kwargs):
    conn = kwargs.get("conn", None)
    nb = NetworkBuilder(conn, config.get("STANDARD","SFCBR0_PATH"))
    build = nb.local_management_net()

#just for internat development to build the local manage bridge
# Beware: deploys every vm at once
def deploy_vm(**kwargs):
    conn = kwargs.get("conn", None)
    name = kwargs.get("vmName", None)
    vmb = VmBuilder(conn, vmName=name)
    build = vmb.defineVM()
    return build

def getVMsofVNF(name, **kwargs):
    conn = kwargs.get("conn", None)
    helper = Helper()
    vms = helper.getVmsfromVnf(name)
    return vms

#init sfcbr0
def build_sfc_network(**kwargs):
    conn = kwargs.get("conn", None)
    nb = NetworkBuilder(conn, config.get("STANDARD","SFCBR0_PATH"))

    # define ovs with ovs-vsctl for initial config
    # not needed if already defined and then ignored
    print("config state is {0}".format(config.get("STANDARD","UCPE_STATE")))
    if config.get("STANDARD","UCPE_STATE") == "initial":
        run_hyp_cmd(command="ovs-vsctl add-br sfcbr0")
        # link to controller
        nb.build_sfc_net()
        run_hyp_cmd(command="ovs-vsctl set-controller sfcbr0 tcp:10.10.100.2:6633")
        # connect the uCPE-Manager after init SFC-Switch
        run_hyp_cmd(command="ovs-vsctl add-port sfcbr0 manage2 tag=5")


    else:
        nb.build_sfc_net()
        # connect the uCPE-Manager after init SFC-Switch
        run_hyp_cmd(command="ovs-vsctl add-port sfcbr0 manage2 tag=5")

    print("a reboot is required!")
    for i in range(10,0, -1):
       print("time to reboot:", i, " - To abort press ^C")
       #Do your code here
       time.sleep(1)
    os.system("reboot")


#helper to run local hypervisor commands via ssh
def run_hyp_cmd(**kwargs):
    command = kwargs.get("command", None)
    manIp = config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
    hypSshUser = config.get("STANDARD","HYPERVISOR_SSH_USER")
    hypSshPw = config.get("STANDARD","HYPERVISOR_SSH_PW")
    with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
        message = sshconn.runcommand(command)
        print(message) # return nothing if ovs bridge was added, or message if something else happens




# start Thread for getting ucpe_config if not configured
if config.get("STANDARD", "UCPE_STATE") == "initial":
    click.echo("ucPE in initial state try to get ucpe config")
    click.echo("start Config Getter Thread")

    if os.path.isfile(config.get("STANDARD","CONFIG_GETTER_PID_FILE")):
        pidfile = open(config.get("STANDARD","CONFIG_GETTER_PID_FILE"), "r")
        pid = (pidfile.read())
        click.echo("%s already exists, please stop first with pid %s" % (config.get("STANDARD","CONFIG_GETTER_PID_FILE"), pid) )
        rtc = subprocess.call(["kill", "{0}".format(pid)])
        rtc = subprocess.call(["rm", "/tmp/config_getter.pid"])
        #sys.exit()
    #else:
    configGetter = subprocess.Popen(["python3","/opt/management/python_manage/nethinks_manage/configuration/config_get.py"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1) #comment before stdout to see errors
    click.echo("started config getter as subprocess process with pid %s" % configGetter.pid)
    cfg_pid = configGetter.pid
    pidfile = open(config.get("STANDARD","CONFIG_GETTER_PID_FILE"), 'w+')
    pidfile.write(str(cfg_pid))


# start Thread for own API


if os.path.isfile(config.get("STANDARD","API_PID_FILE")):
    pidfile = open(config.get("STANDARD","API_PID_FILE"), "r")
    pid = (pidfile.read())
    click.echo("%s api already exists, kill now %s" % (config.get("STANDARD","API_PID_FILE"), pid) )
    rtc = subprocess.call(["kill", "{0}".format(pid)])
    rtc = subprocess.call(["kill", "{0}".format(int(pid)+1)])
    rtcrm = subprocess.call(["rm", "/tmp/api.pid"])
    #os.killpg(os.getpgid(int(pid)), signal.SIGTERM)
    #sys.exit()
rtcDev = subprocess.call(["export FLASK_ENV=development"], shell=True)
rtcApp = subprocess.call(["set FLASK_APP=/opt/management/python_manage/nethinks_manage/api/app.py"], shell=True)
#apiP = subprocess.Popen(["flask run -h 0.0.0.0 -p 5000" ], cwd="/opt/management/python_manage/nethinks_manage/api/", shell=True, stdout=subprocess.PIPE)#, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)
apiP = subprocess.Popen(["flask run --host=0.0.0.0 --port=5000"],
                                    stdout=subprocess.DEVNULL,
                                    shell=True,
                                    cwd="/opt/management/python_manage/nethinks_manage/api/",
                                    preexec_fn=os.setsid)

click.echo("started API as subprocess process with pid %s" % apiP.pid)
api_pid = apiP.pid
pidfile = open(config.get("STANDARD","API_PID_FILE"), 'w+')
pidfile.write(str(api_pid))
