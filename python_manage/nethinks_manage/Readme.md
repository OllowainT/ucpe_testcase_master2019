nethinks_manage is a CLI Tool to Manage the Nethinks uCPE Architecture
It is proposed to:
- open a specific SSH Tunnel to Hypervisor
- establish a Connection from monarch to Hypervisor
- mount the hypervisor datastore
- download iamges and other files to the hypervisor datastore via ssh and wget
- manage priority zebra (frr) command via -c option (actually only running-config)
- ... add more


To get help just type nethinks_manage --help or nethinks_manage [command] --help

Samples for the modules:

#enable --> default pw: password1!
# activates Zebra NIC to KVM Hypervisor
nethinks_manage enable --password 111



#disable --> default pw: password1!
# disconnect Zebra NIC to KVM Hypervisor
nethinks_manage disable --password 111

# zebra
# shows the running-config
# not ready yet
nethinks_manage zebra




# tunnel
# establish a ssh tunnel via Port 50500 to Hypervisor over monarch
# possible args: status, launch, destroy
# starts a daemon as subprocess  

# launch
nethinks_manage tunnel -c launch -p 111

# launch
nethinks_manage tunnel -c destroy -p 111

# status
nethinks_manage tunnel -c status -p 111



# datastore
# mount/unmount a datastore to specific dir
# possible args: mount, unmount
# -k --> ssh_key

#mount
nethinks_manage datastore -c mount -p password1! -u root
nethinks_manage datastore -c mount

#unmount
nethinks_manage datastore -c unmount



# transfer
# possible methods: ssh, url
# transfers file from path/filename to
# LOCAL_DATASTORE_MOUNT_PATH = "/mnt/datastore_hypervisor"

#easy way tot test transfer test.txt via ssh (default values)
nethinks_manage transfer

# per url:
nethinks_manage transfer -m url -s http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso

# per ssh:
nethinks_manage -m ssh -s 192.168.1.202 -pa /tmp/ -f test.txt -u root -p password1!





# weiter bei test
