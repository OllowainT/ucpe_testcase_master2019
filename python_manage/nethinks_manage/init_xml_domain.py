"""
This module is used to create a simple xml file for a new domain
and also for networks

"""
import sys

DATASTORE_IMAGEPATH = "/datastore"
# import xml.etree.ElementTree as et

class XmlDescriptor():

    def __init__(self, connection):#, name, filepath):
        # self.name = name
        # self.filepath = filepath
        # self.memory = memory
        # self.vcpu = vcpu
        # self.diskpath = diskpath
        # self.interface_type = interface_type
        # self.interface_mac = interface_mac
        # self.os = os
        self.conn = connection

    # <uuid>c7a5fdbd-cdaf-9455-926a-d65c16db1809</uuid>
    #524288‬

                # <disk type ='file' device='cdrom'>
                #     <driver name='qemu' type='raw'/>
                #     <source file='/datastore/iso/mini.iso'/>
                #     <target dev='hda'/>
                # </disk>

                # <interface type='bridge'>
                #     <mac address='52:54:00:00:00:06'/>
                #     <source bridge='sfcbr0'/>
                #     <virtualport type='openvswitch'/>
                #     <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
                # </interface>

                # test1 interfaces
                                # <interface type='direct' name='wan0'>
                                #     <mac address='52:54:00:00:02:00'/>
                                #     <source mode='passthrough' dev='ens40'/>
                                #     <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
                                # </interface>



        self.xml_test1_description = """
        <domain type='kvm'>
            <name>test1</name>
            <memory unit='KiB'>500000</memory>
            <vcpu>1</vcpu>
            <os>
                <type arch="x86_64">hvm</type>
            </os>
            <features>
                <acpi />
                <apic />
                <vmport state="off" />
            </features>
            <clock offset='utc'/>
            <on_poweroff>preserve</on_poweroff>
            <on_reboot>restart</on_reboot>
            <on_crash>destroy</on_crash>
            <devices>
                <emulator>/usr/bin/kvm-spice</emulator>
                <disk type='file' device='disk'>
                  <driver name='qemu' type='qcow2' cache='none'/>
                  <source file='{directory}/iso/test.qcow2'/>
                  <target dev='vda' bus='virtio'></target>
                </disk>
<!--                <hostdev mode='subsystem' type='pci' managed='yes'>
                    <source>
                      <address domain='0x0000' bus='0x02' slot='0x09' function='0x0'/>
                    </source>
                </hostdev>-->
                <interface type='bridge' name='sfc0'>
                    <mac address='52:54:00:00:0a:01'/>
                    <source bridge='sfcbr0'/>
                    <virtualport type='openvswitch'/>
                    <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
                </interface>
                <serial type='pty'>
                    <target type='isa-serial' port='0'/>
                    <model name='isa-serial' />
                </serial>
                <console type='pty'>
                    <target type='serial' port='0'/>
                </console>
                <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'/>
            </devices>
        </domain>""" .format(directory=DATASTORE_IMAGEPATH)


        self.xml_test2_description = """
        <domain type='kvm'>
            <name>test2</name>
            <memory unit='KiB'>500000</memory>
            <vcpu>1</vcpu>
            <os>
                <type arch="x86_64">hvm</type>
            </os>
            <features>
                <acpi />
                <apic />
                <vmport state="off" />
            </features>
            <clock offset='utc'/>
            <on_poweroff>preserve</on_poweroff>
            <on_reboot>restart</on_reboot>
            <on_crash>destroy</on_crash>
            <devices>
                <emulator>/usr/bin/kvm-spice</emulator>
                <disk type='file' device='disk'>
                  <driver name='qemu' type='qcow2' cache='none'/>
                  <source file='{directory}/iso/test2.qcow2'/>
                  <target dev='vda' bus='virtio'></target>
                </disk>
                <interface type='direct' name='lan0'>
                    <mac address='52:54:00:00:01:00'/>
                    <source mode='passthrough' dev='ens41'/>
                    <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
                </interface>
                <interface type='bridge' name='sfc1'>
                    <mac address='52:54:00:00:0a:00'/>
                    <source bridge='sfcbr0'/>
                    <virtualport type='openvswitch'/>
                    <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
                </interface>
                <serial type='pty'>
                    <target type='isa-serial' port='0'/>
                    <model name='isa-serial' />
                </serial>
                <console type='pty'>
                    <target type='serial' port='0'/>
                </console>
                <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'/>
            </devices>
        </domain>""" .format(directory=DATASTORE_IMAGEPATH)



        self.xml_test11_description = """
        <domain type='kvm'>
            <name>test11</name>
            <memory unit='KiB'>500000</memory>
            <vcpu>1</vcpu>
            <os>
                <type arch="x86_64">hvm</type>
            </os>
            <features>
                <acpi />
                <apic />
                <vmport state="off" />
            </features>
            <clock offset='utc'/>
            <on_poweroff>preserve</on_poweroff>
            <on_reboot>restart</on_reboot>
            <on_crash>destroy</on_crash>
            <devices>
                <emulator>/usr/bin/kvm-spice</emulator>
                <disk type='file' device='disk'>
                  <driver name='qemu' type='qcow2' cache='none'/>
                  <source file='{directory}/iso/test11.qcow2'/>
                  <target dev='vda' bus='virtio'></target>
                </disk>
                <serial type='pty'>
                    <target type='isa-serial' port='0'/>
                    <model name='isa-serial' />
                </serial>
                <console type='pty'>
                    <target type='serial' port='0'/>
                </console>
                <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'/>
            </devices>
        </domain>""" .format(directory=DATASTORE_IMAGEPATH)


        self.initXmlDomain = """
        <domain type='kvm'>
            <name>test11</name>
            <memory unit='KiB'>500000</memory>
            <vcpu>1</vcpu>
            <os>
                <type arch="x86_64">hvm</type>
            </os>
            <features>
                <acpi />
                <apic />
                <vmport state="off" />
            </features>
            <clock offset='utc'/>
            <on_poweroff>preserve</on_poweroff>
            <on_reboot>restart</on_reboot>
            <on_crash>destroy</on_crash>
            <devices>
                <emulator>/usr/bin/kvm-spice</emulator>
                <disk type='file' device='disk'>
                  <driver name='qemu' type='qcow2' cache='none'/>
                  <source file='{directory}/iso/test11.qcow2'/>
                  <target dev='vda' bus='virtio'></target>
                </disk>
                <serial type='pty'>
                    <target type='isa-serial' port='0'/>
                    <model name='isa-serial' />
                </serial>
                <console type='pty'>
                    <target type='serial' port='0'/>
                </console>
                <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'/>
            </devices>
        </domain>"""


#02:09.0
#pci_0000_02_09_0

# <device>
#   <name>pci_0000_02_09_0</name>
#   <path>/sys/devices/pci0000:00/0000:00:11.0/0000:02:09.0</path>
#   <parent>pci_0000_00_11_0</parent>
#   <driver>
#     <name>e1000</name>
#   </driver>
#   <capability type='pci'>
#     <domain>0</domain>
#     <bus>2</bus>
#     <slot>9</slot>
#     <function>0</function>
#     <product id='0x100f'>82545EM Gigabit Ethernet Controller (Copper)</product>
#     <vendor id='0x8086'>Intel Corporation</vendor>
#     <iommuGroup number='5'>
#       <address domain='0x0000' bus='0x02' slot='0x07' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x00' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x03' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x06' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x09' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x02' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x08' function='0x0'/>
#       <address domain='0x0000' bus='0x00' slot='0x11' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x01' function='0x0'/>
#       <address domain='0x0000' bus='0x02' slot='0x04' function='0x0'/>
#     </iommuGroup>
#   </capability>
# </device>

    def init_vm(self):
        ## mount first vnf
        domain = self.conn.createXML(self.xml_test1_description, 0)
        if domain == None:
            print('Failed to create a domain from an XML definition.', file=sys.stderr)
            exit(1)
        print('Guest '+domain.name()+' has booted')

        ## mount second vnf
        domain = self.conn.createXML(self.xml_test2_description, 0)
        if domain == None:
            print('Failed to create a domain from an XML definition.', file=sys.stderr)
            exit(1)
        print('Guest '+domain.name()+' has booted')

        # nur testweise:
        domain = self.conn.createXML(self.xml_test11_description, 0)
        if domain == None:
            print('Failed to create a domain from an XML definition.', file=sys.stderr)
            exit(1)
        print('Guest '+domain.name()+' has booted')

        self.conn.close()
        exit(0)

# virsh attach-interface --domain test11 --type direct --source ens40 --mac 52:54:00:00:02:00 --config --live
#
# virsh attach-interface --domain test11 --type network --source sfcbr0 --mac 52:54:00:00:02:00 --config --live --target
#
# virsh detach-interface --domain test1 --type direct --mac 52:54:00:00:02:00 --config --live
#
#                         <interface type='direct' name='wan0'>
#                             <mac address='52:54:00:00:02:00'/>
#                             <source mode='passthrough' dev='ens40'/>
#                             <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
#                         </interface>
#                         type=direct,source=ens34,source_mode=passthrough,mac=52:54:00:00:00:01
