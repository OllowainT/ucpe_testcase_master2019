#!/usr/bin/env python3
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn
from ipaddress import IPv4Network
from ruamel.yaml import YAML
from ruamel.yaml.scalarstring import SingleQuotedScalarString, DoubleQuotedScalarString
import sys
import os
import subprocess
import configparser
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn
from nethinks_manage.logger import Logger
from nethinks_manage.datastore import datastoreController
import shutil

logger = Logger(__name__)
# class for defining the cloud init for ubuntu:
# use netplan and Networking config 2
class Cloud_initializer(object):
    def __init__(self, vmObj):
        self.name = "Cloud_initializer"
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
        self.initConf = dict(ethernets={}, version=2)
        self.meta_data = """instance-id: {0}
local-hostname: {0}""".format(vmObj.name)

        self.user_data = """#cloud-config

write_files:
  - path: /tmp/test.txt
    content: |
      test ist dies

runcmd:
    - ["netplan", "apply"]
    - ["cp", "/run/systemd/network/*.link", "/etc/systemd/network/"]
    - ["update-initramfs", "-u"]
    - reboot"""

        self.network_config = ""
        # add passthroigh interfaces to network
        for pt_interface in vmObj.pt_interfaceList:
            self.__addInterface(pt_interface)
        # add normal interfaces to network
        for n_interface in vmObj.n_interfaceList:
            self.__addInterface(n_interface)

        self.__writefiles() #write 3 files for cloud-init
        self.__genIso() # generate iso out of the 3 files
        self.__copyIsoToHyp(vmObj) # copy and rename cloud init iso

    def __copyIsoToHyp(self, vmObj):
        ds = datastoreController()
        ds.mountDatastore()
        logger.info("copying...")
        with open("/tmp/config.iso", 'rb') as fsrc:
            with open(self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/vms/" + vmObj.name + ".config.iso", 'wb') as fdest:
                shutil.copyfileobj(fsrc, fdest, 16 * 1024)
        ds.unmount_datastore()

    def __genIso(self):
        subprocess.run(["genisoimage","-output", "config.iso", "-volid", "cidata", "-joliet", "-rock", "user-data", "meta-data", "network-config"], cwd="/tmp")
        logger.info("generate cloud-init iso as 'config.iso'")

    def __writefiles(self):
        yaml = YAML()
        with open("/tmp/network-config", "w") as netcon:
            yaml.indent(mapping=4, sequence=6, offset=2)
            yaml.dump(self.network_config, netcon)
        with open("/tmp/meta-data", "w") as metadat:
            metadat.write(self.meta_data)
        with open("/tmp/user-data", "w") as userdat:
            userdat.write(self.user_data)

    def __addInterface(self, iObj):

        yaml = YAML()
        intName = str(iObj.name)
        fstring = "false"
        interfaceDict = {"match":{"macaddress": "{0}".format(iObj.mac)},"set-name":"{0}".format(iObj.name),"addresses":["{0}/{1}".format(iObj.ip, str(self.__subnetToPrefix(iObj.subnet)))],"dhcp4": False}
        yaml.indent(mapping=4, sequence=6, offset=1)
        self.initConf["ethernets"][intName] = interfaceDict
        self.initConf["ethernets"][intName]["match"]["macaddress"] = SingleQuotedScalarString(iObj.mac)
        yaml.dump(self.initConf, sys.stdout)
        self.network_config = self.initConf
        logger.info("added netplan interface {0}".format(iObj.name))


    def __subnetToPrefix(self, subnet):
        prefix = IPv4Network('0.0.0.0/{0}'.format(subnet)).prefixlen
        return prefix

    def buildStartupDisk(self):
        manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
        hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")

        #genisoimage -output config.iso -volid cidata -joliet -rock user-data meta-data network-config

        buildIsoCmd = ""
        with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
                message = sshconn.runcommand()
                print(message)
