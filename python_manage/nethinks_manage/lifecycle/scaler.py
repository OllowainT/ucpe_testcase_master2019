#!/usr/bin/env python3
# file encoding must be LF, not CRLF!
from threading import Thread
import sys
import utils
import os
import subprocess
import configparser
from nethinks_manage.logger import Logger
import libvirt
import time
from collections import deque
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn



class ScalerManager(Thread):
    def __init__(self,type, name, down_threshold, duration, max, min, path, step, up_threshold, vmName):
        #self.name = "ScalerManager"
        self.connection = None
        self.scType = type
        self.pid = os.getpid()
        self.vmMaxMemory = None
        self.scName = name
        self.logger = Logger("ScalerThread_"+self.scName+ "_" +__name__)
        self.min = min
        self.max = max
        self.step = step
        self.duration = duration
        self.up_threshold = up_threshold
        self.down_threshold = down_threshold
        self.vmName = vmName
        self.dom = None
        self.measures = deque(maxlen=int((int(self.duration)/5)))
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
        self.__open_virsh_connection()


        self.__calcThreshold()


    def __del__(self):
        # destroy here?
        rtc = subprocess.call(["rm", "/tmp/{0}".format(self.scName)])#,"{0}".format(int(self.pid)-1)])
        #ready = await rtc.stdout
        print("delete self")

    def startRamScaler(self):
        pass

    def get_memory_actual(self):
        """get memory stats with virsh commands
        """
        virshcmd = "virsh qemu-monitor-command"
        qmp_actual = -1
        cmd = "%s %s '{ \"execute\": \"qom-get\", \"arguments\":  { \"path\": \"/machine/peripheral/balloon0\",  \"property\": \"guest-stats\" }}'" % (virshcmd, self.vmName)
        self.logger.info("check memory stats with virsh command: %s" % cmd)

        # run command on hypervisor:
        manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
        hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")
        with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
            message = sshconn.runcommand(cmd)
            print(message)


        # ret, out = utils.exec_cmd(cmd, shell=True)
            out_dict = eval(message)
            if 'return' in out_dict:
                if 'stats' in out_dict['return']:
                    avail_mem = out_dict['return']['stats']['stat-available-memory']
                    maxMem = out_dict['return']['stats']['stat-total-memory']
                    self.vmMaxMemory = int(maxMem)/1024/1024
                    print("total Mem", self.vmMaxMemory)
                    print("avail_mem", avail_mem)
                    avail_mem= int(avail_mem)/1024/1024
                    print("avail_mem", avail_mem)
                    print("self.max", self.max)
                    used_mem = int(self.vmMaxMemory) - avail_mem
                    print("used_mem", used_mem)
                    self.measures.append(used_mem*100/int(self.vmMaxMemory) ) #percentage of memory
                    print("Memory Used in percent is", self.measures[-1])
                    return int(used_mem) # full memory usage in MB
        # else:
        #     return False
        #
        # if qmp_actual == -1:
        #     return False
        #
        # logger.info("the memory actual is: %s" % qmp_actual)
        # return qmp_actual



    def __getMemoryOfVM(self): # get memory by vmName with libvirt
        self.dom.SetMemoryStatsPeriod = 1
        stats  = self.dom.memoryStats()

        memoryAbsolute = stats['actual']
        print('memory current:', memoryAbsolute)

        #self.measures.append((int(memoryAbsolute)/1024)*100/int(self.vmMaxMemory)) #percentage of memory
        return int(memoryAbsolute)/1024 # full memory usage in MB


    def increaseMemory(self, mem_value):
        print("increase mem")
        manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
        hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")
        increaseCmd = "virsh setmem {0} {1}M --live".format(self.vmName, int(mem_value))
        with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
            message = sshconn.runcommand(increaseCmd)
            print(message)
        print("New Memory Value for {0} is {1}M".format(self.vmName, mem_value))

    def decreaseMemory(self, mem_value):
        print("decrease mem")
        manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
        hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")
        increaseCmd = "virsh setmem {0} {1}M --live".format(self.vmName, mem_value)
        with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
            message = sshconn.runcommand(increaseCmd)
            print(message)
        print("New Memory Value for {0} is {1}M".format(self.vmName, mem_value))

    def __calcThreshold(self):
        try:
            self.dom = self.connection.lookupByName(self.vmName)
            print("found domain: ", self.vmName)
        except:
            print("{0} not found".format(self.vmName))

        time.sleep(int(self.config.get("STANDARD", "SCALER_START_DELAY")))
        while self.dom.isActive():
            actMemUsage = self.get_memory_actual()
            print("used mem:", actMemUsage)
            memUsage = 0
            #calc last duration usage percantage in mean
            balloon = self.__getMemoryOfVM()
            print("balloon is", balloon)

            for measure in self.measures:

                    memUsage = memUsage + measure/len(self.measures)
            if len(self.measures) == self.measures.maxlen:
                print("memUsage Overall is", memUsage)
                if memUsage >= int(self.up_threshold):
                    # check if act usage + step is not over max
                    print("Memory usage of last {0}s is over threshold of defined {1}".format(self.duration, self.up_threshold))
                    if int(self.max) >= balloon + int(self.step):# (int(self.max) + int(self.step)):
                    #if actMemUsage +int(self.step) <= int(self.max):
                        value = balloon + int(self.step)#int(self.vmMaxMemory) + int(self.step)
                        #value = actMemUsage + int(self.step)
                        self.increaseMemory(int(value))
                elif int(self.max) <= int(self.down_threshold):
                    # check if act usage - step is not under min
                    print("Memory usage of last {0}s is under threshold of defined {1}".format(self.duration, self.down_threshold))
                    if actMemUsage <= balloon - int(self.step):# (int(self.min) - int(self.step)):
                        value = balloon - int(self.step) # int(self.vmMaxMemory) - int(self.step)
                        self.decreaseMemory(value)
                time.sleep(5)
        if not self.dom.isActive():
            self.__del__()

    def __open_virsh_connection(self, **kwargs):
        def request_cred(credentials, user_data):
            for credential in credentials:
                if credential[0] == libvirt.VIR_CRED_AUTHNAME:
                    credential[4] = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
                elif credential[0] == libvirt.VIR_CRED_PASSPHRASE:
                    credential[4] = self.config.get("STANDARD","HYPERVISOR_SSH_PW")
            return 0

        auth = [[libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_PASSPHRASE], request_cred, None]


        conn = libvirt.openAuth("qemu+ssh://%s/system"%(self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")), auth, 0)
        if conn == None:
            print('Failed to open connection', file=sys.stderr)
            return False
            exit(1)

        self.logger.info("connection to qemu established...\n")

        self.connection = conn


        # while vmName active
        # __getMemoryOfVM()
        # calc THreshold and push value to queue every 5 sec and only hold max duration/5 values
        # calc overAllUsedMemory
        # if value is over up_threshold
            # add ram by step __setMemory
        # if value is under down_threshold
            # remove ram by step __setMemory

if __name__ == "__main__":
    type = sys.argv[1]
    name = sys.argv[2]
    down_threshold= sys.argv[3]
    duration = sys.argv[4]
    max = sys.argv[5]
    min = sys.argv[6]
    path = sys.argv[7]
    step = sys.argv[8]
    up_threshold = sys.argv[9]
    vmName = sys.argv[10]
    print("vmName is",vmName)
    kvm_connection = ScalerManager(
    type, name, down_threshold, duration, max, min, path, step, up_threshold, vmName) #order counts!
    #kvm_connection.run()
