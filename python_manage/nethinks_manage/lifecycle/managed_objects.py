import sys
import os
import uuid
import xml.dom.minidom
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn
from nethinks_manage.logger import Logger
import xml.etree.ElementTree as ET
import configparser


logger = Logger(__name__)

class VMObj(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get("name", None)
        self.category = kwargs.get("category", None)
        self.imagefile = kwargs.get("imagefile", None)
        self.imagetype = kwargs.get("imagetype", None)
        self.imagepath = kwargs.get("imagepath", None)
        self.memory = kwargs.get("memory", None)
        self.start_memory = kwargs.get("start_memory", None)
        self.vcpu = kwargs.get("vcpu", None)
        self.uuid = self.__generateUuid()
        self.description = kwargs.get("description", None)
        self.scalerList = []
        self.n_interfaceList = [] #holds a list of all normal Interface Objects
        self.pt_interfaceList = [] #holds a list of all passthrough Interface Objects
        self.xmlRepr = None
        self.fullXmlFilepath = ""
        self.vmType = kwargs.get("type", None)


        self.__buildVMXML()

    def __str__(self):
        return "name: {0}, category: {1}, memory: {2}, vcpus: {3}, ...".format(self.name, self.category, self.memory, self.vcpu)

    def getXMLRepresentation(self):
        #self.__buildVMXML() # build vm to xml file
        return self.xmlRepr


    def __generateUuid(self):
        newuuid = uuid.uuid1()
        return str(newuuid)

    def __buildVMXML(self):
        """
        generates a domain specific xml file to define it via kvm
        """
        xmlInit = """
        <domain type='kvm'>
            <name>{name}</name>
            <memory unit='MiB'>{memory}</memory>
            <currentMemory unit='MiB'>{cmemory}</currentMemory>
            <vcpu>{vcpus}</vcpu>
            <os>
                <type arch="x86_64">hvm</type>
            </os>
            <features>
                <acpi />
                <apic />
                <vmport state="off" />
            </features>
            <clock offset='utc'/>
            <on_poweroff>preserve</on_poweroff>
            <on_reboot>restart</on_reboot>
            <on_crash>destroy</on_crash>
            <devices>
                <emulator>/usr/bin/kvm-spice</emulator>
                <disk type='file' device='disk'>
                  <driver name='qemu' type='qcow2' cache='none'/>
                  <source file='/datastore/vms/{name}.qcow2'/>
                  <target dev='vda' bus='virtio'></target>
                </disk>
                {cdromDesc}
                <serial type='pty'>
                    <target type='isa-serial' port='0'/>
                    <model name='isa-serial' />
                </serial>
                <console type='pty'>
                    <target type='serial' port='0'/>
                </console>
                <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'/>
                <memballoon model='virtio' autodeflate='on'>
                    <stats period='5'/>
                </memballoon>
            </devices>
            <imageinit type='{imagetype}' path='{imagepath}'/>
            <vminfo type='{vmType}'/>
            <scalers>
            </scalers>
        </domain>""".format(name=self.name, memory=self.memory, vcpus=self.vcpu,
        imagetype=self.imagetype, imagepath=self.imagepath, vmType=self.vmType,
        cdromDesc=self.__appendCdrom(self.vmType, self.name), cmemory=self.start_memory)



        dom = xml.dom.minidom.parseString(xmlInit)
        self.xmlRepr = dom.toxml()

    def __appendCdrom(self, vmType, vmName):
        """
        check for vmtype and append the correct Cdromdrive or nothing if type is manual
        """
        print("type of vm for cdrom is", vmType)
        cdromDesc = ""
        if vmType == "ubuntu-cloud":
            cdromDesc = """
                <disk type='file' device='cdrom'>
                  <source file='/datastore/vms/{name}.config.iso'/>
                  <target dev='hdb'/>
                  <readonly/>
                </disk>
            """.format(name=vmName)

        # todo: implement iosxe and others
        return cdromDesc



    #add interfaces obj for a vm to self list
    def appendInterface(self, interface):
        logger.debug("append interface type: {0}".format(interface.type))
        if type(interface) is VMInterface:
            logger.info("interface type is VMInterface")
            self.n_interfaceList.append(interface)
        elif type(interface) is VMPtInterface:
            logger.info("interface type is VMPtInterface")
            self.pt_interfaceList.append(interface)
        else:
            logger.error("wrong Type for this method, please give a Interface Object")
            raise TypeError("wrong Type for this method, please give a Interface Object")
        # add interface as
        self.appendInterfaceXml(interface)
        #self.__buildVMXML()

    #add scaler obj for a vm to self list
    def appendScaler(self, scalerObj):
        logger.debug("append scaler type: {0}".format(scalerObj.scType))
        if scalerObj.scType == "ramscaler":
            logger.info("scaler type is ram scaler")
            self.scalerList.append(scalerObj)
        elif scalerObj.scType == "cpuscaler":
            logger.info("interface type is cpu scaler")
            logger.warm("scaler type is not supported right now")
        else:
            logger.error("wrong Type for this method, please give a Interface Object")
            raise TypeError("wrong Type for this method, please give a Interface Object")
        # add interface as
        self.appendScalerXml(scalerObj)


    def appendInterfaceXml(self, interface):
        """ Add interface to vm xml """
        logger.info("found {0} in InterfaceList --> add to XML Description".format(interface.name))
        vmRoot = ET.fromstring(self.xmlRepr)
        interf = ET.fromstring(interface.xmlRepresentation())
        devices = vmRoot.find("devices")
        devices.append(interf)
        #logger.debug(ET.dump(vmRoot))
        self.xmlRepr = ET.tostring(vmRoot, method='xml', encoding='unicode') # write to xml string

    def appendScalerXml(self, scalerObj):
        """ Add scaler to vm xml """
        logger.info("found {0} in scalerList --> add to XML Description".format(scalerObj.scName))
        vmRoot = ET.fromstring(self.xmlRepr)
        scaleXml = ET.fromstring(scalerObj.getXmlRepr())
        devices = vmRoot.find("scalers")
        devices.append(scaleXml)
        #logger.debug(ET.dump(vmRoot))
        self.xmlRepr = ET.tostring(vmRoot, method='xml', encoding='unicode') # write to xml string



class VMInterface(object):
    def __init__(self, **kwargs):
        self.mac = kwargs.get("mac", None)
        self.name = kwargs.get("name", "")
        self.type = kwargs.get("type", None)
        self.ip = kwargs.get("ip", None)
        self.subnet = kwargs.get("subnet", None)
        self.portgroup = kwargs.get("portgroup", None)
        self.gateway = kwargs.get("gateway", None)
        self.xmlRepr = ""
        self.__IntBuildXmlRepr()
        #print(self.xmlRepresentation())


    def __IntBuildXmlRepr(self):
        # check if interface type sfc or manage (other switches)
        if self.type == "sfc":
            logger.info("build interface xml for {0} of type {1}".format(self.name, self.type))
            self.xmlRepr = """
                <interface type='network'>
                  <mac address='{mac}'/>
                  <source network='sfcbr0' portgroup='{pg}'/>
                  <virtualport type='openvswitch'/>
                  <target dev='{name}'/>
                  <model type='rtl8139'/>
                </interface>
            """.format(mac=self.mac, pg=self.portgroup, name=self.name)

        elif self.type == "manage":
            logger.info("build interface xml for {0} of type {1}".format(self.name, self.type))
            self.xmlRepr = """
                <interface type='network'>
                  <mac address='{mac}'/>
                  <source network='sfcbr0' portgroup='management'/>
                  <virtualport type='openvswitch'/>
                  <target dev='{name}'/>
                  <model type='rtl8139'/>
                </interface>
            """.format(mac=self.mac, name=self.name)


        else:
            self.xmlRepr = "type not supported"



    def xmlRepresentation(self):
        #dom = xml.dom.minidom.parseString(self.xmlRepr)
        #print(dom.toxml())
        #return dom.toprettyxml()
        return self.xmlRepr


class VMPtInterface(VMInterface):
    def __init__(self, **kwargs):
        super(VMPtInterface, self).__init__(**kwargs)
        self.hw_interface = kwargs.get("hw_interface", None)
        # unmount from hypervisor
        self.__IntPtBuildXmlRepr()
        #self.managed = kwargs.get("managed", True)
        #super(VMPtInterface, self).__buildXmlRepr()

    def __IntPtBuildXmlRepr(self):
        logger.info("build interface xml for {0} of type {1}".format(self.name, self.type))
        # check if interface type sfc or manage (other switches)
        if self.type == "passthrough":
            logger.info("build interface xml for {0} of type {1}".format(self.name, self.type))

            if self.hw_interface == "ens160":
                self.xmlRepr = """
                    <hostdev mode='subsystem' type='pci' managed='no'>
                      <alias name='{name}'/>
                      <hw_interface name='{hw_interface}'/>
                      <mac address='{mac}'/>
                      <source>
                        <address domain='0x0000' bus='0x03' slot='0x00' function='0x00'/>
                      </source>
                    </hostdev>
                """.format(mac=self.mac, name=self.name, hw_interface=self.hw_interface)

            elif self.hw_interface == "ens192":
                self.xmlRepr = """
                    <hostdev mode='subsystem' type='pci' managed='no'>
                      <alias name='{name}'/>
                      <hw_interface name='{hw_interface}'/>
                      <mac address='{mac}'/>
                      <source>
                        <address domain='0x0000' bus='0x0b' slot='0x00' function='0x00'/>
                      </source>
                    </hostdev>
                """.format(mac=self.mac, name=self.name, hw_interface=self.hw_interface)

        else:
            self.xmlRepr = "type not supported pt"

class MemScaler(object):
    def __init__(self, name, scaleroptions):
        self.scName = name
        self.scType = scaleroptions.get("type", None)
        self.min = scaleroptions.get("min", None)
        self.max = scaleroptions.get("max", None)
        self.step = scaleroptions.get("step", None)
        self.duration = scaleroptions.get("duration", None)
        self.up_threshold = scaleroptions.get("up_threshold", None)
        self.down_threshold = scaleroptions.get("down_threshold", None)
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))

    def getXmlRepr(self):
        filepath = "{0}ram_{1}".format(self.config.get("STANDARD", "SCALER_PID_PATH"), self.scName)
        xml = """ <scaler type='ramscaler' path='{path}' name='{scName}' min='{min}' max='{max}' step='{step}' duration='{duration}' up_threshold='{up_threshold}' down_threshold='{down_threshold}' /> """.format(
        path=filepath, scName=self.scName, min=self.min, max=self.max, step=self.step,
        duration=self.duration,  up_threshold=self.up_threshold, down_threshold=self.down_threshold)
        return xml



class VL(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get("name", None)
        self.connectionpoints = kwargs.get("cplist", None)
        self.vlan = kwargs.get("vlan", None)
        self.network = kwargs.get("network", None)
        self.subnet = kwargs.get("subnet", None)

#to define sfcnetwork
class SfcNetwork(object):
    def __init__(self, **kwargs):
        self.name = "sfcNetwork"
        self.vls = []
        self.sfcXml = """
        <network>
            <name>sfcbr0</name>
            <forward mode='bridge'/>
            <bridge name='sfcbr0'/>
            <virtualport type='openvswitch'/>
            <portgroup name='management'>
                <vlan>
                <tag id='5'/>
                </vlan>
            </portgroup>
        </network>
        """
        self.sfcInitXml = """
        <network>
            <name>sfcbr0</name>
            <forward mode='bridge'/>
            <bridge name='sfcbr0'/>
            <virtualport type='openvswitch'/>
            <portgroup name='management'>
                <vlan>
                <tag id='5'/>
                </vlan>
            </portgroup>
        </network>
        """

    def addPortgroup(self, **kwargs):
        pgName = kwargs.get("pgName", None)
        vlanId = kwargs.get("vlanId", None)

        xml = """
        <portgroup name='{0}'>
            <vlan>
            <tag id='{1}'/>
            </vlan>
        </portgroup>
        """.format(pgName, vlanId)

        # add to xml
        root = ET.fromstring(self.sfcXml)
        #pg = ET.SubElement(root,xml)
        pg = ET.fromstring(xml)
        #logger.debug(ET.dump(root))
        #root.insert(0,pg)
        root.append(pg)
        #logger.debug(ET.dump(root))
        self.sfcXml = ET.tostring(root, method='xml', encoding='unicode') # write to xml string
        #print (self.sfcXml)

    def getSfcNetworkXML(self):
        return self.sfcXml

    def getInitSfcNetworkXML(self):
        return self.sfcInitXml


class PassthroughUnmounter(object):
    """ takes the pt interface cp and dismounts it from hypervisor
    """
    def __init__(self, ptname):
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
        self.ptInterfaceName = ptname
        self.pciAddress = None
        self.hypervisorId = None
        self.__findPciAddress(self.ptInterfaceName)
        self.__unmountPasstroughIntFromHost(self.pciAddress, self.hypervisorId)


    def __findPciAddress(self, interfaceName):
        if interfaceName == "ens160":
            self.pciAddress = "0000:03:00.0"
            self.hypervisorId = "15ad 07b0"
        elif interfaceName == "ens192":
            self.pciAddress = "0000:0b:00.0"
            self.hypervisorId = "15ad 07b0"



    def __unmountPasstroughIntFromHost(self, pciAddress, hypervisorId):
        manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
        hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
        hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")

        cmdList = []
        cmd1 = "echo '{0}' > /sys/bus/pci/drivers/pci-stub/new_id".format(hypervisorId)
        cmd2 = "echo '{0}' > /sys/bus/pci/devices/{0}/driver/unbind".format(pciAddress)
        cmd3 = "echo '{0}' > /sys/bus/pci/drivers/pci-stub/bind".format(pciAddress)
        cmd4 = "virsh nodedev-detach {0}".format(self.__pciAddressToPciName(pciAddress))
        cmdList.append(cmd1)
        cmdList.append(cmd2)
        cmdList.append(cmd3)
        cmdList.append(cmd4)
        #command = "subproccess.run({0})".format(cmd1)
        print(cmd1)

        with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
            for com in cmdList:
                message = sshconn.runcommand(com)
                print(message)
        # subprocess.run(["echo","'{0}'".format(hypervisorId), ">", "/sys/bus/pci/drivers/pci-stub/new_id" ])
        # subprocess.run(["echo","'{0}'".format(pciAddress), ">", "/sys/bus/pci/devices/{0}/driver/unbind".format(pciAddress) ])
        # subprocess.run(["echo","'{0}'".format(pciAddress), ">", "/sys/bus/pci/drivers/pci-stub/bind" ])
        # subprocess.run(["virsh","nodedev-detach", self.__pciAddressToPciName(pciAddress)])

    def __pciAddressToPciName(self, pciAddress):
        if pciAddress == "0000:03:00.0":
            return "pci_0000_03_00_0"
        elif pciAddress == "0000:0b:00.0":
            return "pci_0000_0b_00_0"
