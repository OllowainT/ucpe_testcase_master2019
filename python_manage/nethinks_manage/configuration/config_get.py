#!/usr/bin/python3

from nethinks_manage.logger import Logger
import configparser
import requests
from requests.exceptions import HTTPError
import os
import sys
import subprocess
import time
from ruamel.yaml import YAML
import json

logger = Logger("ConfigGetter"+__name__ )

class ConfigGetter(object):
    def __init__(self):
        self.maxTries = 100000 # only for devellopment - delete for production
        self.tries = 0

        self.name = "ConfigGetter"
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
        #self.config = open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini', 'w')

    def __del__(self):
        print("delete self, because ucpe state is not initial")
        subprocess.call(["rm", "/tmp/config_getter.pid"])
        sys.exit(0)

    def getConfigFromManager(self):
        managerIp = self.config.get("STANDARD", "MANAGER_IP")
        managerPort = self.config.get("STANDARD", "MANAGER_PORT")
        ucpeId = self.config.get("STANDARD", "UCPE_ID")
        while self.config.get("STANDARD", "UCPE_STATE") == "initial":
            logger.info("try to get config from Manager")
            try:
                response = requests.get("http://{0}:{1}/config/{2}".format(managerIp, managerPort, ucpeId))

                # If the response was successful, no Exception will be raised
                response.raise_for_status()
            except HTTPError as http_err:
                logger.info(f'HTTP error occurred: {http_err}')  # Python 3.6
            except Exception as err:
                logger.info(f'Other error occurred: {err}')  # Python 3.6
            else:
                logger.info('Success!')
                config = response.json()
                if len(config) > 0:
                    # config_update = configparser.ConfigParser.RawConfigParser()
                    logger.info("got config from manager, update ucpe_config and write")

                    with open(self.config.get("STANDARD", "UCPE_CONFIGPATH"),"w" ) as ucpe_config:
                        # building one multidoc yaml out of json an write back
                        yaml = YAML()
                        yamldocs = list()
                        for obj in config:
                            yamldocs.append(obj)
                        #multiConfig = yaml.dump_all(yamldocs)
                        yaml.dump_all(yamldocs, ucpe_config)


                    self.config.set("STANDARD", "UCPE_STATE", "configured")
                    with open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini', 'w') as wConfig:
                        self.config.write(wConfig)
                #config = requests.get("http://127.0.0.1:6000/config/id/1")
            if self.tries < self.maxTries:
                time.sleep(5)
            else:
                logger.info("config getter timeout reached")
                self.__del__()

        if self.config.get("STANDARD", "UCPE_STATE") != "initial":
            self.__del__()




if __name__ == '__main__':
    cg = ConfigGetter()
    cg.getConfigFromManager()
