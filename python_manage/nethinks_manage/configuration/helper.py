#!/usr/bin/env python3
import yaml
import configparser
from nethinks_manage.logger import Logger

logger = Logger(__name__)


class Helper(object):
    def __init__(self):
        self.name = "Helper"
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))

    def getVmsfromVnf(self, vnfName):
        with open(self.config.get("STANDARD", "UCPE_CONFIGPATH")) as ucpeConf:
            config = yaml.safe_load_all(ucpeConf)
            conf = list(config)
            foundOne = False
            vnflist = []
            vms = []
            for doc in conf:
                if "vnfs" in doc.keys():
                    vnflist = doc.get("vnfs")
                    print(vnflist)
                    logger.info("key vnfs found and saved")
                    foundOne = True
            if foundOne:
                for vnfname, vmdef in vnflist.items():
                    print(vnfname)
                    print(vmdef)
                    if vnfname == vnfName:
                        print("searched vnf name is: {0} and found is: {1}".format(vnfname, vnfName))
                        for name, options in vmdef.items():
                            logger.info("vnfname: {0}".format(name))
                            vms.append(name)

            if not foundOne:
                logger.warning("there is no vnfs part of config")
            return vms
