#!/usr/bin/env python3
import yaml
import sys
import json
from nethinks_manage.logger import Logger
from nethinks_manage.lifecycle.managed_objects import VMObj, VMPtInterface, VMInterface, SfcNetwork
from nethinks_manage.lifecycle.cloud_init import Cloud_initializer
from nethinks_manage.lifecycle.managed_objects import MemScaler

logger = Logger(__name__)
# todo read this out of properties file
#SFCBR0_PATH = "/opt/management/python_manage/nethinks_manage/network/sfcbr0.xml"

class ConfigParser(object):
# handles the config file to a specific object. Target is to define topology

    def __init__(self, filepath, sfcbr0Path, vmPath):
        # debug: import web_pdb; web_pdb.set_trace()

        self.config = None
        self.vnflist = None
        self.cplist = None
        self.networkconfig = None
        self.sfcNetworkXml = ""
        self.vllist = None
        self.sfcbr0Path = sfcbr0Path
        self.vmPath = vmPath
        self.imageconfig = None
        self.imageList = None
        self.scalers = None

        #self.__parseImages()
        self.__readConfig(filepath) # first read in the uCPE config
        self.__parseScalersDoc()
        #the order of parsing the config is necessary:
        self.__parseNetworkDoc() # parse and build the Network
        self.__parseVNFDoc() # parse and build the VMs for each vnf





    # read uCPE config and save as list
    def __readConfig(self, filepath):
        configstream = open(filepath, 'r')
        config = yaml.safe_load_all(configstream)
        self.config = list(config)


    def __parseNetworkDoc(self):
        for doc in self.config:
            # decide type of document to build objects:
            if "network" in doc.keys():
                """
                holds the whole network definition part (own doc)
                """
                self.networkconfig = doc.get("network")
                logger.debug(self.networkconfig)
                self.__saveVLList()
                logger.info("key network found and saved")
                self.__parseSfcNetwork()
                self.__writeSfcNetworkXML() # write to sfc xml file

    def __parseScalersDoc(self):
        for doc in self.config:
            # decide type of document to build objects:
            if "scalers" in doc.keys():
                """
                holds the whole scaler definition part (own doc)
                """
                self.scalers = doc.get("scalers")
                print("Scalerlist: ", self.scalers)
                #self.__saveScalerList()
                # self.__saveVLList()
                # logger.info("key network found and saved")
                # self.__parseSfcNetwork()
                # self.__writeSfcNetworkXML() # write to sfc xml file

    def __parseVNFDoc(self):
        foundOne = False
        for doc in self.config:
            if "vnfs" in doc.keys():
                self.vnflist = doc.get("vnfs")
                print(self.vnflist)
                logger.info("key vnfs found and saved")
                self.__saveCPList() # get cps
                self.__parseVM()
                foundOne = True
            if not foundOne:
                logger.warning("there is no vnfs part of config")

    def __getCPinfo(self, cpname):
        # get info for specific cp name
        cplistcopy = self.cplist.copy()
        logger.debug("cpname: {0}".format(cpname))
        try:
            cpconfig = cplistcopy.pop("{0}".format(cpname))
            logger.debug("cpname: {0}".format(cpconfig))
            single_cp_dict = {"{0}".format(cpname): cpconfig} # give the key value as dict
            return single_cp_dict
        except KeyError:
            logger.error("Configuration error: cant find CP '{0}'".format(cpname))
            raise

    #def __saveScalerList(self):
        # foundscaler = False
        # scalerconf = self.scalers
        # print(self.scalers)
        # for scalername, scaleroptions in scalerconf.items():
        #         print("scalername: ", scalername)
        #         #self.vllist = nwconf.get("vls")
        #         foundscaler = True
        #
        # if not foundscaler:
        #     logger.error("cant find scalers in config")
        #     raise ConfigDefException("cant find scalers in config")
        # else:
        #     logger.info("found scalers in network config")
        #     logger.debug("found scalers: {0} ".format(self.scalers))

    def __saveVLList(self):
        foundvls = False
        nwconf = self.networkconfig
        for nwpart, nwpartlist in nwconf.items():
            if "vls" == nwpart:
                print("vls: ", nwpart)
                self.vllist = nwconf.get("vls")
                foundvls = True

        if not foundvls:
            logger.error("cant find vls in network config")
            raise ConfigDefException("cant find vls in network config")
        else:
            logger.info("found vls in network config")
            logger.debug("foundvls: {0} ".format(self.vllist))

    def __saveCPList(self):
        foundcps = False
        nwconf = self.networkconfig
        for nwpart, nwpartlist in nwconf.items():
            if "cps" == nwpart:
                print("cps: ", nwpart)
                self.cplist = nwconf.get("cps")
                foundcps = True

        if not foundcps:
            logger.error("cant find cps in network config")
            raise ConfigDefException("cant find cps in network config")
        else:
            logger.info("found cps in network config")
            logger.debug("foundcps: {0} ".format(self.cplist))

    def __parseVM(self):
        vm = None
        for vnfname, vmdef in self.vnflist.items():
            print(vnfname)
            print(vmdef)
            for name, options in vmdef.items():
                logger.info("vnfname: {0}".format(name))
                logger.info("options: {0}".format(json.dumps(options, indent=2)))
                #get vmname:
                #vm = self.__buildVM(name, options)
                # check if there are nics to configure if no got way with name and options,
                # if yes parse network CPs and give as param
                if "connectionpoints" in options.keys():
                    if len(options.get("connectionpoints")) > 1:
                        vnf_cplist_info = []
                        logger.info("vm with connectionspoints given for {0}".format(name))
                        vnf_cplist = options.get("connectionpoints")
                        for cp in vnf_cplist:
                            cpinfo = self.__getCPinfo(cp) # give the VM Builder a cp list
                            vnf_cplist_info.append(cpinfo)
                            logger.info("cpinfo {0}".format(cpinfo))
                        logger.info("vnf_cplist_info: {0}".format(json.dumps(vnf_cplist_info, indent=2)))
                        vm = self.__buildVM(name, options, cpconfigs=vnf_cplist_info)
                    else:
                        logger.error("there are not enough cps listed in config")
                        raise ConfigDefException("there are not enough cps listed in config")
                else:
                    logger.info("no connectionspoints given for {0}".format(name))
                    #
                    vm = self.__buildVM(name, options)
                self.__writeVmXML(vm)
                # todo: check for type:
                if vm.vmType == "ubuntu-cloud":
                    self.__writeAutoDeployUbuntu(vm)


    def __buildVM(self, name, options, **kwargs):
        """
        build a vm object with given name and params
        """
        imagefile = options.get("imagefile")
        imagefileName = imagefile.get("name")
        imageInitSource = imagefile.get("init_src")
        imageInitSourceType = list(imageInitSource.keys())[0]
        imageInitSourcePath = imageInitSource.get(imageInitSourceType)
        # print("source: {0}".format(imageInitSource))
        # print("sourcetype: {0}".format(imageInitSourceType))
        # print("sourcePath: {0}".format(imageInitSourcePath))
        # print("313221: {0}".format(imagefile.get("name")))
        category = options.get("category")
        memory = options.get("max_memory")
        start_memory = options.get("start_memory")
        vcpu = options.get("vcpu")
        description = options.get("description")
        type = options.get("type")
        scalers = options.get("scalers")
        print("scalers are: ", scalers)
        #all required params:
        vm = VMObj(name=name, imagefile=imagefileName, category=category,
        memory=memory, vcpu=vcpu, description=description, imagetype=imageInitSourceType,
        imagepath=imageInitSourcePath, type=type, start_memory=start_memory)

        for scaler in scalers:
            scalerObj = self.__buildScaler(scaler)
            print(scalerObj)
            if scalerObj is None:
                logger.error("this type of scaler is not supported right now")
            elif scalerObj.scType == "ramscaler":
                vm.appendScaler(scalerObj)
            else:
                logger.error("this type of scaler is not supported right now")

        # todo: add imagefile path

        # all optional params:
        # add interfaces:
        cpconfigs = kwargs.get("cpconfigs", None)
        if cpconfigs is not None:
            for interface in cpconfigs:
                interface_obj = self.__buildInterface(interface)
                vm.appendInterface(interface_obj)



        #logger.info("new vm object generated: {0}".format(str(vm)))
        logger.debug("vm as xml: {0}".format(vm.getXMLRepresentation()))
        #logger.debug("interfaces: {0}".format(str(vm.n_interfaceList)))
        return vm

    def __buildScaler(self, scaler):
        logger.info("building Scaler")
        sc = None
        # get scaler out of list:
        if scaler in self.scalers.keys():
            print("Scaler is:" ,scaler)
            scaleroptions = self.scalers.get(scaler)
            print(scaleroptions)
            if scaleroptions.get("type") == "ramscaler": #only ramscaler supported
                sc = MemScaler(scaler, scaleroptions)
        return sc

    def __buildInterface(self, interface):
        logger.info("building Interface...")
        interface_params = interface.items()
        interface_cpname = None
        for ifname in interface.keys():
            interface_cpname = ifname

        value_dict = dict()
        vm_if = None
        # to enter the first entry and get if values:
        for key, value in interface_params:
            value_dict = value

        # append values:
        if len(value_dict) > 0:
            mac = value_dict.get("mac")
            name = value_dict.get("name")
            ip = value_dict.get("ip")
            type = value_dict.get("type")
            subnet = value_dict.get("subnet")
            gw = value_dict.get("gateway", None)
            # check for interface type
            if value_dict.get("type") == "passthrough":
                hw_interface = value_dict.get("hw_interface")
                #managed = value_dict.get("managed")
                vm_if = VMPtInterface(mac=mac, name=name, type=type, subnet=subnet,
                ip=ip, hw_interface=hw_interface, gateway=gw) #managed=managed
            elif value_dict.get("type") == "manage":
                vm_if = VMInterface(mac=mac, name=name, subnet=subnet, type=type, ip=ip, gateway=gw)
            else:
                #search for Portgroup = VL in config
                pg = self.__findPortgroup(interface_cpname)
                vm_if = VMInterface(mac=mac, name=name, subnet=subnet, type=type, ip=ip, portgroup=pg, gateway=gw)
            return vm_if
        else:
            logger.error("interface values not present for {0}".format(interface.keys()))
            raise ConfigDefException("interface values not present for {0}".format(interface.keys()))

    def __findPortgroup(self, name):
        logger.info("search for cp {0} in VLs".format(name))
        found = False
        for vlname, options in self.vllist.items():
            if "connectionpoints" in options.keys():
                cps = options.get("connectionpoints")
                if name in cps:
                    logger.info("found interface {0} in VL {1}".format(name, vlname))
                    found = True
                    return vlname
            else:
                logger.error("the sfc interface {0} isnt listed in a portgroup, but has to. Please check config".format(name))
                raise ConfigDefException("the sfc interface {0} isnt listed in a portgroup, but has to. Please check config".format(name))
        if not found:
            raise ConfigDefException("the sfc interface {0} isnt listed in a portgroup, but has to. Please check config".format(name))

    def __writeVmXML(self, vmObj):
        """ write VM Config to xml file"""
        vmObj.fullXmlFilepath = "{0}/{1}.xml".format(self.vmPath, vmObj.name)
        logger.info("Try to write VM XML Config to {0}".format(vmObj.fullXmlFilepath))
        with open(vmObj.fullXmlFilepath, "w") as vmfile:
            #print(self.vmObj.getXMLRepresentation())
            vmfile.write(vmObj.getXMLRepresentation())
        logger.info("VM XML File config file written to Path {0}".format(vmObj.fullXmlFilepath))

    def __writeAutoDeployUbuntu(self, vmObj):
        """ write autodeploy files for ubuntu"""
        # build cloud-init object
        ci = Cloud_initializer(vmObj)
        # write the 3 files
        # generate iso file
        # append iso to vm xml
        vmObj.fullXmlFilepath = "{0}/{1}.xml".format(self.vmPath, vmObj.name)
        logger.info("Try to write VM XML Config to {0}".format(vmObj.fullXmlFilepath))
        with open(vmObj.fullXmlFilepath, "w") as vmfile:
            #print(self.vmObj.getXMLRepresentation())
            vmfile.write(vmObj.getXMLRepresentation())
        logger.info("VM XML File config file written to Path {0}".format(vmObj.fullXmlFilepath))


    def __parseSfcNetwork(self):
        sfcSwitch = SfcNetwork()
        #build sfc xml config
        for name, options in self.vllist.items():
            print(name)
            print(options)
            # the cps are not interesting for the SFCNetwork and are ignored
            if "vlan" in options.keys():
                vlan = options.get("vlan")
                sfcSwitch.addPortgroup(pgName=name, vlanId=vlan)
            else:
                logger.warning("there is no vlan for %s in config" % name)

        self.sfcNetworkXml = sfcSwitch.getSfcNetworkXML()






    def __writeSfcNetworkXML(self):
        """ write SFC Network Switch Config to file"""
        with open(self.sfcbr0Path, "w") as sfcfile:
            print(self.sfcNetworkXml)
            sfcfile.write(str(self.sfcNetworkXml))
        logger.info("SFC Switch config file written")

class MyException(Exception):
    pass


class ConfigDefException(MyException):
    def __init__(self, msg):
        super().__init__(msg)


if __name__ == "__main__":
    if sys.argv is None:
        path = "../ucpe_config.yml"
    else:
        path = sys.argv[0]
    cp = ConfigParser(path)
