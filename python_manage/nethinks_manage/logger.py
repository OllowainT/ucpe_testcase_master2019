import logging
import os


class Logger(object):

    def __init__(self, name, level=logging.DEBUG):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        self.modulepath = path = os.path.abspath(__file__)

        fh = logging.FileHandler(
            '{0}/../logs/nethinks_manage.log'.format(self.modulepath), 'a')
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s[%(levelname)s]: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

        sh = logging.StreamHandler()
        cmdformatter = logging.Formatter(
            '%(asctime)s %(name)s[%(levelname)s]: %(message)s', datefmt='%H:%M:%S')
        sh.setFormatter(cmdformatter)
        self.logger.addHandler(sh)

    def debug(self, msg):
        self.logger.debug(msg)

    def info(self, msg):
        self.logger.info(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def error(self, msg):
        self.logger.error(msg, exc_info=True)

    def critical(self, msg):
        self.logger.critical(msg)


if __name__ == '__main__':
    log = Logger()
    log.debug('debug')
    log.info('info')
    log.warning('warning')
    log.error('error')
    log.critical('critical')
