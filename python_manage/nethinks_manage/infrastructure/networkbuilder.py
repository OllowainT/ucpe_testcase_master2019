import os
import sys
import subprocess
import configparser

from nethinks_manage.logger import Logger


logger = Logger(__name__)

class NetworkBuilder(object):

    def __init__(self, conn, sfcbr0Path):
        logger.info("created class NetworkBuilder")
        self.name = "networkbuilder"
        self.sfcbr0Path = sfcbr0Path
        self.conn = conn
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))

    def local_management_net(self):

        #not working yet because just defined with ovs-vsctl manually
        with open("/opt/management/python_manage/nethinks_manage/network/local-management.xml") as lanxml:

            try:
                network = self.conn.networkLookupByName('lmanagement')
                if network.isActive():
                    network.destroy()
                if network.isPersistent():
                    network.undefine()
            except:
                print("lmanagement not found")

            lswitch = lanxml.read()
            swdom = self.conn.networkDefineXML(lswitch)
            if swdom is None:
                print('Failed to create a network from an XML definition.', file=sys.stderr)
                exit(1)
            swdom.create()
            if swdom.isActive():
                swdom.setAutostart(1)
                print('The new persistent virtual network is active and set to autostart')
                #followed must be run manually:
                #rcode = subprocess.call("ovs-vsctl add-br lmanagement", shell=True)

            else:
                print('The new persistent virtual network is not active')

    def build_sfc_net(self):
        # todo: Problem is. if the sfcbr0 is reconfigured every machine loses connection.
        # this could be solved with libvirt hooks or some other scripting for each interface connected to ovs
        # https://serverfault.com/questions/322536/kvm-guests-lose-connectivity-after-networking-restart
        # maybe only destroy if a

        ###
        # start ryu controller instance:
        if os.path.isfile(self.config.get("STANDARD","RYU_PID_FILE")):
            pidfile = open(self.config.get("STANDARD","RYU_PID_FILE"), "r")
            pid = (pidfile.read())
            logger.info("%s already exists, please stop first with pid %s" % (self.config.get("STANDARD","RYU_PID_FILE"), pid) )
        else:
            ryuCont = subprocess.Popen(["ryu-manager", "/opt/management/python_manage/nethinks_manage/ovs-controller/example_switch_14.py"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)
            logger.info("started ryu Manager with pid %s" % ryuCont.pid)
            ryuCont_pid = ryuCont.pid
            with open(self.config.get("STANDARD","RYU_PID_FILE"), 'w+') as pidfile:
                pidfile.write(str(ryuCont_pid))
        ####

        logger.info("open sfcbr0 config file in %s" % self.config.get("STANDARD","SFCBR0_PATH"))
        with open(self.config.get("STANDARD","SFCBR0_PATH")) as lanxml:

            try:
                network = self.conn.networkLookupByName('sfcbr0')
                if network.isActive():
                    network.destroy()
                if network.isPersistent():
                    network.undefine()
            except:
                logger.info("sfcbr0 not found")

            lswitch = lanxml.read()
            swdom = self.conn.networkDefineXML(lswitch)
            if swdom is None:
                logger.info('Failed to create a network from an XML definition.', file=sys.stderr)
                exit(1)
            swdom.create()
            if swdom.isActive():
                swdom.setAutostart(1)
                logger.info('The new persistent virtual network is active and set to autostart')
                # add via ovs-ctl:
                #rcode = subprocess.call("ovs-vsctl add-br sfcbr0", shell=True)

            else:
                logger.info('The new persistent virtual network is not active')
