#!/usr/bin/env python3
import sys
import os
import xml.dom.minidom
from nethinks_manage.datastore import datastoreController
import configparser
import shutil
import subprocess
import pyfastcopy
import yaml
from nethinks_manage.lifecycle.managed_objects import PassthroughUnmounter
from nethinks_manage.lifecycle.scaler import ScalerManager
from nethinks_manage.infrastructure.ssh_comm import Ssh_HyperConn

from nethinks_manage.logger import Logger


logger = Logger(__name__)

class VmBuilder(object):
    def __init__(self, conn, **kwargs):
        self.name = "VmBuilder"
        self.vmName = kwargs.get("vmName", None)
        self.vmList = kwargs.get("vmList", None)
        self.connection = conn
        self.config = configparser.ConfigParser()
        self.config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))

        # check if there is a Passthrough Device in xml def:
        self.__checkForPtDevice()


        # decide if there is a single build or a list build:
        if self.vmName is not None:
            self.__checkForImage(self.vmName)
        elif self.vmList is not None:
            print("todo: implement list vm mount")

    # def getVmsfromVnf(self, vnfName):
    #     with open(self.config.get("STANDARD","UCPE_CONFIGPATH")) as ucpeConf:
    #         config = yaml.safe_load_all(ucpeConf)
    #         conf = list(config)
    #         foundOne = False
    #         vnflist = []
    #         vms = []
    #         for doc in conf:
    #             if "vnfs" in doc.keys():
    #                 vnflist = doc.get("vnfs")
    #                 print(vnflist)
    #                 logger.info("key vnfs found and saved")
    #                 foundOne = True
    #         if foundOne:
    #             for vnfname, vmdef in vnflist.items():
    #                 print(vnfname)
    #                 print(vmdef)
    #                 print("searched vnf name is: {0} and found is: {1}".format(vnfname, vnfName))
    #                 if vnfname == vnfName:
    #                     print("searched vnf name is: {0} and found is: {1}".format(vnfname, vnfName))
    #                     for name, options in vmdef.items():
    #                         logger.info("vmname: {0}".format(name))
    #                         vms.append(name)
    #
    #         if not foundOne:
    #             logger.warning("there is no vnfs part of config")
    #         return vms


    def __checkForImage(self, vmName):
        # mount datastore
        ds = datastoreController()
        ds.mountDatastore()
        # check if image is there
        foundfile = False
        print("check for image")
        datastorePath = os.path.dirname(self.config.get("STANDARD", "LOCAL_DATASTORE_MOUNT_PATH"))
        for root, dirs, files in os.walk(datastorePath):
            for file in files:
                if file == vmName + ".qcow2":
                    logger.info("found file {0}.qcow2".format(vmName))
                    foundfile = True
                    #maybe adjust to not recursive mode



        # if yes defineVM
        if foundfile:
            logger.info("defineVM")
        else:
            # if not: load image from init_src to datastore and defineVm
            logger.info("load image from init source")

            dom = xml.dom.minidom.parse(self.config.get("STANDARD","VMXML_PATH") + "/" + self.vmName + ".xml")
            initImageType = dom.getElementsByTagName("imageinit")[0]
            print(initImageType.toxml())
            initType = initImageType.getAttribute("type")
            initPath = initImageType.getAttribute("path")
            if initType == "localpathname":
                self.__loadImageLocal(initPath)
            elif initType == "url":
                logger.info("not implemented")
            else:
                logger.error("wrong type set in config")

            logger.info("defineVM")

        ds.unmount_datastore()

    def __loadImageLocal(self,  initPath):
        # copy image from local datastore to vms path with new name
        logger.info("copying...")
        with open(self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/iso/" + initPath, 'rb') as fsrc:
            with open(self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/vms/" + self.vmName + ".qcow2", 'wb') as fdest:
                shutil.copyfileobj(fsrc, fdest, 16 * 1024 * 1024* 2)
        # shutil.copyfileobj(self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/iso/" + initPath,
        # self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/vms/" + self.vmName + ".qcow2", 16 * 1024 * 1024)
        logger.info("copy file {0} from {1} to {2}".format(initPath, self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/iso/" + initPath,self.config.get("STANDARD","LOCAL_DATASTORE_MOUNT_PATH") + "/vms/" + self.vmName + ".qcow2"))

    def defineVM(self):
        success = False
        #copy xml from monarch to datastore
        with open(self.config.get("STANDARD","VMXML_PATH") + "/" + self.vmName + ".xml") as vmxml:

            try:
                dom = self.connection.lookupByName(self.vmName)
                if dom.isActive():
                    dom.destroy()
                if dom.isPersistent():
                    dom.undefine()
            except:
                print("{0} not found".format(self.vmName))

            vm = vmxml.read()
            domain = self.connection.defineXML(vm)
            if domain == None:
                print('Failed to create a domain from an XML definition.', file=sys.stderr)
                exit(1)
            domain.create()
            if domain.isActive():
                domain.setAutostart(1)
                success = True
                print('The new persistent virtual domain is active and set to autostart')
                print('Guest '+domain.name()+' has booted')
            else:
                print('The new persistent virtual domain is not active')

            logger.info("load image from init source")


            # check for scaler and start:
            logger.info("check for scalers to build...")
            dom = xml.dom.minidom.parseString(vm)
            scalers = dom.getElementsByTagName("scalers")
            for node in scalers:
                if len(node.childNodes) >= 1:
                    scalers = node.getElementsByTagName("scaler")
                    for scaler in scalers:
                        logger.info("found scaler type: {0}, name {1}".format(scaler.getAttribute("type"), scaler.getAttribute("name")))
                        if scaler.getAttribute("type") == "ramscaler":
                            # get all other infos
                            name = scaler.getAttribute("name")
                            down_threshold = scaler.getAttribute("down_threshold")
                            duration = scaler.getAttribute("duration")
                            max = scaler.getAttribute("max")
                            min = scaler.getAttribute("min")
                            path = scaler.getAttribute("path")
                            step = scaler.getAttribute("step")
                            type = scaler.getAttribute("type")
                            up_threshold = scaler.getAttribute("up_threshold")
                            # check if scaler already up
                            # if yes delete and rebuild
                            scPathName = "{0}{1}".format(self.config.get("STANDARD","SCALER_PID_PATH"), name)
                            if os.path.isfile(scPathName):
                                pidfile = open(scPathName, "r")
                                pid = (pidfile.read())
                                print("%s already exists, try to kill with pid %s" % (scPathName, pid) )
                                manIp = self.config.get("STANDARD","HYPERVISOR_MANAGEMENT_IP")
                                hypSshUser = self.config.get("STANDARD","HYPERVISOR_SSH_USER")
                                hypSshPw = self.config.get("STANDARD","HYPERVISOR_SSH_PW")
                                with Ssh_HyperConn(manIp, hypSshUser, hypSshPw) as sshconn:
                                    message = sshconn.runcommand("kill {0}".format(pid))
                                    print(message)
                                    sshconn.runcommand("rm {0}".format(scPathName))
                            else:
                                # start scaler daemon for this vm
                                sc = subprocess.Popen(["/opt/management/python_manage/nethinks_manage/lifecycle/scaler.py ramscaler {0} {1} {2} {3} {4} {5} {6} {7} {8}".format( name,
                                down_threshold, duration, max, min, path, step, up_threshold, self.vmName) ], shell=True)#, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)
                                #print(sc.pid)
                                scalerpid = sc.pid
                                pidfile = open(scPathName, 'w+')
                                pidfile.write(str(scalerpid))
                                # ramscaler = ScalerManager(type, self.connection, name=name, dt=down_threshold,
                                # duration=duration, max=max, min=min, path=path, step=step,
                                # ut=up_threshold)
            # print(initImageType.toxml())
            # initType = initImageType.getAttribute("type")
            # initPath = initImageType.getAttribute("path")

            return success

    def __checkForPtDevice(self):
        """ if there is a pt device unmount it from hypervisor"""
        print("checking for hostdevs")
        dom = xml.dom.minidom.parse(self.config.get("STANDARD","VMXML_PATH") + "/" + self.vmName + ".xml")
        devices = dom.getElementsByTagName("devices")[0]

        for hostdev in devices.getElementsByTagName("hostdev"):
            print ("found hostdev device")
            hard = hostdev.getElementsByTagName("hw_interface")[0]
            intName = hard.getAttribute("name")
            print("intename",intName)
            # unmount from hypervisor
            unmount = PassthroughUnmounter(intName)
        # search for hostdev
