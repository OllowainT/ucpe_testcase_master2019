#!/usr/bin/env python3

import paramiko
from nethinks_manage.logger import Logger

logger = Logger(__name__)

class Ssh_HyperConn(object):

    def __init__(self, ip, user, pw):
        logger.info("created class Ssh_HyperConn")
        self.name = "Ssh_HyperConn"
        self.ip = ip
        self.user = user
        self.pw = pw
        self.ssh = paramiko.SSHClient()

    def __enter__(self):
        logger.info
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(self.ip, username=self.user, password=self.pw)
        logger.info("created ssh connection")
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.ssh.close()
        logger.info("closed ssh connection")

    def runcommand(self, cmdString):
        logger.info("try to run command %s" % cmdString)
        try:
            ssh_stdin, ssh_stdout, ssh_stderr = self.ssh.exec_command(cmdString, timeout=3, get_pty=True)
            rel_message = ssh_stdout.read()
            return rel_message

        except paramiko.AuthenticationException:
                print("Authentication failed when connecting to %s" % self.ip)
        except:
                print("Could not SSH to %s with error %s" % self.ip, ssh_stderr)
