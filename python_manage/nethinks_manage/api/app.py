from flask import Flask, request
from flask_cors import CORS
from nethinks_manage.logger import Logger
import json
import configparser
from ruamel.yaml import YAML

logger = Logger(__name__)


app = Flask(__name__)
app.config["DEBUG"] = True
config = configparser.ConfigParser()
config.read_file(open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini'))
CORS(app)

@app.route('/')
def index():
  return 'Server Works!'

@app.route('/config', methods=['POST'])
def storeConfig():
    if request.method == 'POST':
        cfg = request.get_json()
        if len(cfg) > 0:
            logger.info("got config from manager, update ucpe_config and write")
            with open(config.get("STANDARD", "UCPE_CONFIGPATH"),"w" ) as ucpe_config:
                # building one multidoc yaml out of json an write back
                yaml = YAML()
                yamldocs = list()
                for obj in cfg:
                    yamldocs.append(obj)

                yaml.dump_all(yamldocs, ucpe_config)

            # set state:
            config.set("STANDARD", "UCPE_STATE", "configured")
            with open('/opt/management/python_manage/nethinks_manage/configuration/uCPE_configfile.ini', 'w') as wConfig:
                config.write(wConfig)
            return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
        else:
            return json.dumps({'success':False}), 500, {'ContentType':'application/json'}

@app.route('/status', methods=['GET'])
def getState():
    if request.method == 'GET':
        return "status"

@app.route('/build', methods=['GET'])
def buildConfig():
    if request.method == 'GET':

        return "build config"
