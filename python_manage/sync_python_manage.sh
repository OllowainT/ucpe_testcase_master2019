#!/bin/sh


# sync python_manage dir to monarch and run dev script
rsync -r -a -v  --exclude '__pycache__' --exclude 'nethinks.manage.egg-info' --delete --progress -e 'ssh -i "/mnt/c/Users/Markus Betz/.ssh/id_rsa"' "/mnt/c/Users/Markus Betz/Documents/Masterarbeit/Testumgebung/python_manage" root@192.168.1.203:/opt/management/
ssh -i "/mnt/c/Users/Markus Betz/.ssh/id_rsa" root@192.168.1.203 "/opt/management/python_manage/build_dev_package.sh"
