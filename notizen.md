Die Testumgebung für die VMs der Thesis wird im VLAN 134 im Netz 212.218.100.0/24
betrieben.
Zu Beginn stehen 3 IP-Adressen zur Verfügung, die im Internet erreichbar sind:
- 212.218.100.101
Dient als Projekt-Management-VM, auf welcher Scripts und weitere Anwendungen
zur Verfügung gestellt werden.

- 212.218.100.102
simuliert das uCPE
ucpe1-nbg1
- ssh aktiviert und root ssh-able gemacht

Installation kvm:
 - sudo apt-get install qemu-kvm
 - adduser root kvm

--> Snapshot erstellt ("kvm frisch")

- apt install qemu libvirt-bin  bridge-utils  virt-manager
(https://www.linuxtechi.com/install-configure-kvm-ubuntu-18-04-server/)
Bridged löschen geht nicht über netplan!!! siehe --> https://itrig.de/index.php?/archives/2354-netplan-unter-Ubuntu-Server-18.04-LTS-konfigurieren-oder-entfernen.html
ip link delete dev br0


Um den Scaler zu testen wird mittels stress-ng und dem Befehl
stress-ng --vm 1 --vm-bytes 112m --vm-method all
auf ein Ubuntu Testgerät der Speicher auf 81% ausgelastet, wodurch der Up-Scaler starten sollte.
Die aktuelle Speicherausnutzung lässt sich am Hypervisor mit
virsh dominfo instance1
anzeigen. used memory gibt dabei die aktuelle größe an. max memory den maximalwert

_______________
ucpe2-nbg1:
192.168.1.202
user: nethinks
pw: pass..


Ubuntu server installiert (nicht die live Variante!)
Keyboard-Layout auf Deutsch in File /etc/default/keyboard
Bridged Connection über Host mit IP eingerichtet für Zugriff nach außen (Updates)
apt uppdate/upgrade

sudo apt install openssh-server
Zugriff per ssh auch per root erlaubt

(ipv4 Weiterleitung zwischen interfaces erlauben)
sudo echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sudo sysctl -p /etc/sysctl.conf

(Firewallregeln neu definieren um das weiterleiten aller ip pakete zu erlauben)
sudo apt install -y iptables-persistent
sudo iptables -I FORWARD -j ACCEPT
sudo dpkg-reconfigure -y iptables-persistent


(KVM installieren)
apt install qemu-kvm libvirt-clients libvirt-daemon-system virt-manager cpu-checker libvirt-bin bridge-utils libguestfs-tools libnss-libvirt
sudo systemctl start libvirtd
sudo adduser $USER libvirt

(curl installieren)
apt install curl

(ubuntu server image lokal ablegen)
mkdir /datastore/iso
curl http://cdimage.ubuntu.com/releases/18.04.2/release/ubuntu-18.04.2-server-amd64.iso --output /datastore/iso/ubuntu-18.04.2-server-amd64.iso

adduser `id -un` libvirt-qemu
adduser `id -un` kvm
adduser `id -un` libvirt-dnsmasq


Linux Kernel Read Rechte für alle user setzen(root hat eigentlich Rechte, aber für später anderen User eventuell):
chmod 0644 /boot/vmlinuz*


Der Hypervisor wird aus Gründen der Einfachheit für das Projekt als Datastore verwendet.
Folgende Pfade werden definiert:
- Für Images und ISO-Dateien: /datastore/iso
- Für VMs und deren virtuelle Festplatten, sowie Konfigurationsdateien: /datastore/vms/<vm-name>
Pool Definition anlegen und auf Autostart setzen:
virsh pool-create-as datastore dir --source-path /datastore --target /datastore
#virsh pool-autostart datastore
virsh pool-start datastore


Rechte für Verzeichnis anpassen:
chown -R root:libvirt /datastore
chmod g+w /datastore
chmod o+x /datastore

ACLs anlegen, damit in Zukunfst alle images usw in dem /datastore Verzeichnis ebenfalls der libvirt Grruppe zugriff ermöglichen
setfacl -m g:libvirt:rw /datastore
setfacl -dm g:libvirt:rw /datastore

Selbiges für alle Unterverzeichnisse durchführen

Falls es zu Problemen beim Hostnamen auflösen der Gast-VMs kommt kann ein NNS genutzt werden.
Ist aber eher nur bei NAT nötig. -- Siehe Part "Enable Libvirt’s NSS plugin" von https://www.brianlinkletter.com/build-a-network-emulator-using-libvirt/


Eine Verbindung zum KVM Hypervisor kann mit Hilfe von virsh folgendermaßen aufgebaut werden:
virsh -c qemu+ssh://nethinks@192.168.1.202/system


_______________________________________________________

#############
# Erstellen der Management-VM "Monarch"
#############
192.168.1.203
user: nethinks
pw: pass...


Starten der Monarch-VM (Management)
Optionen:


works für create über virt-install:
virt-install --connect qemu:///system -n monarch -r 2048 --vcpus=2 --disk path=/datastore/vms/monarch/monarch.qcow2,size=25 --location /datastore/iso/ubuntu-18.04.2-server-amd64.iso  --os-type linux --os-variant ubuntu18.04 --virt-type kvm --network type=direct,source=ens34,source_mode=passthrough,mac=52:54:00:00:00:01 --network type=direct,source=ens39,source_mode=passthrough,mac=52:54:00:00:00:02 -k de --extra-args="console=ttyS0,115200n8 serial,acpi=force" --console pty,target_type=serial --noautoconsole --features acpi=on

ESXi
virt-install --connect qemu:///system -n monarch -r 2048 --vcpus=2 --disk path=/datastore/vms/monarch/monarch.qcow2,size=25 --location /datastore/iso/ubuntu-18.04.2-server-amd64.iso  --os-type linux --os-variant ubuntu18.04 --virt-type kvm --network type=direct,source=ens192,source_mode=passthrough,mac=52:54:00:00:00:01 --network type=direct,source=ens224,source_mode=passthrough,mac=52:54:00:00:00:02 -k de --extra-args="console=ttyS0,115200n8 serial,acpi=force" --console pty,target_type=serial --noautoconsole --features acpi=on

ESXi mit vorhandener Platte
virt-install --connect qemu:///system -n monarch -r 2048 --vcpus=2 --import --disk path=/datastore/vms/monarch/monarch.qcow2,size=25 --os-type linux --os-variant ubuntu18.04 --virt-type kvm --network type=direct,source=ens192,source_mode=passthrough,mac=52:54:00:00:00:01 --network type=direct,source=ens224,source_mode=passthrough,mac=52:54:00:00:00:02 -k de  --console pty,target_type=serial --noautoconsole --features acpi=on

--> enable ssh bei Installation und weise IP zu.

# define net manage Network:
virsh net-define managebr0.xml    --> file siehe tree
virsh net-start managebr0
virsh net-autostart managebr0

# Attach Interface zu monarch:
virsh attach-interface --domain monarch --type network \
        --source managebr0 --model virtio \
        --mac 52:54:00:00:00:AA --config --live




--network type=direct,source=ens40,mac=52:54:00:00:00:03
--graphics none
--boot menu=on,useserial=on,kernel_args="console=/dev/ttyS0"
--noacpi
--noautoconsole

- openSSH Server bei Installation mit installiert
- IP vergeben für Primäre IP 192.168.1.203 für ersteinrichtung

acpi dienst installieren!
"sudo apt-get install acpid"
Add it to default run level:  update-rc.d acpid enable
File anlegen: /etc/acpi/events/powerbtn mit Inhalt: action=/sbin/poweroff

virsh edit monarch und folgende Zeile anpassen:   <on_poweroff>preserve</on_poweroff>
da diese sonst auf destroy steht. Warum auch immer...?!

mit virsh dumpxml monarch > monarch.xml kann die Konfig der VM als xml gespeichert werden

Praktisch: (siehe --boot) https://www.systutorials.com/docs/linux/man/1-virt-install/

XML erstellen:
virsh dumpxml monarch > /datastore/vms/monarch/monarch.xml


Installation FRR (routing)
sudo apt update
sudo apt-get install \
   git autoconf automake libtool make libreadline-dev texinfo \
   pkg-config libpam0g-dev libjson-c-dev bison flex python3-pytest \
   libc-ares-dev python3-dev libsystemd-dev python-ipaddress python3-sphinx \
   install-info build-essential libsystemd-dev libsnmp-dev perl

apt install cmake
apt install libpcre3-dev

git clone https://github.com/CESNET/libyang.git
cd libyang
mkdir build; cd build
cmake -DENABLE_LYD_PRIV=ON -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -D CMAKE_BUILD_TYPE:String="Release" ..
make
sudo make install

apt-get install protobuf-c-compiler libprotobuf-c-dev
apt-get install libzmq5 libzmq3-dev

sudo groupadd -r -g 92 frr
sudo groupadd -r -g 85 frrvty
sudo adduser --system --ingroup frr --home /var/run/frr/ \
   --gecos "FRR suite" --shell /sbin/nologin frr
sudo usermod -a -G frrvty frr


git clone https://github.com/frrouting/frr.git frr
cd frr
./bootstrap.sh
./configure \
    --prefix=/usr \
    --includedir=\${prefix}/include \
    --enable-exampledir=\${prefix}/share/doc/frr/examples \
    --bindir=\${prefix}/bin \
    --sbindir=\${prefix}/lib/frr \
    --libdir=\${prefix}/lib/frr \
    --libexecdir=\${prefix}/lib/frr \
    --localstatedir=/var/run/frr \
    --sysconfdir=/etc/frr \
    --with-moduledir=\${prefix}/lib/frr/modules \
    --with-libyang-pluginsdir=\${prefix}/lib/frr/libyang_plugins \
    --enable-configfile-mask=0640 \
    --enable-logfile-mask=0640 \
    --enable-snmp=agentx \
    --enable-multipath=64 \
    --enable-user=frr \
    --enable-group=frr \
    --enable-vty-group=frrvty \
    --with-pkg-git-version \
    --with-pkg-extra-version=-MyOwnFRRVersion \
    --enable-systemd=yes
make
sudo make install

nano /etc/sysctl.conf
# Uncomment the next line to enable packet forwarding for IPv4
net.ipv4.ip_forward=1

sysctl -p

nano /etc/modules-load.d/modules.conf
# Load MPLS Kernel Modules
mpls_router
mpls_iptunnel

modprobe mpls-router mpls-iptunnel

# Für jedes Interface das mpls aktiviert haben soll muss dies hier durchgeführt werden (Enable MPLS Forwarding)(aktuell übersprungen):
http://docs.frrouting.org/projects/dev-guide/en/latest/building-frr-for-ubuntu1804.html

install -m 644 tools/frr.service /etc/systemd/system/frr.service
systemctl enable frr

nano /etc/frr/daemons   #können die einzelnen deamons gestartet werden -->
ospf=yes
zebra_options=... auf 0.0.0.0 gesetzt

rm /etc/frr/frr.conf

# Es wurde direkt ein User "frr" angelegt mit den entsprechenden Rechten angelegt.
passwd frr     --> pass...
nano /etc/passwd --> Zeile Anpassen:
frr:x:107:92:FRR suite,,,:/var/run/frr/:/usr/bin/vtysh
# dies leitet direkt auf die vtysh um. Wenn man sich nun per ssh unter dem User frr anmeldet kommt man direkt in die zebra Config



nano /etc/services --> folgendes einfügen:
zebrasrv      2600/tcp                 # zebra service
zebra         2601/tcp                 # zebra vty
ripd          2602/tcp                 # RIPd vty
ripngd        2603/tcp                 # RIPngd vty
ospfd         2604/tcp                 # OSPFd vty
bgpd          2605/tcp                 # BGPd vty
ospf6d        2606/tcp                 # OSPF6d vty
ospfapi       2607/tcp                 # ospfapi
isisd         2608/tcp                 # ISISd vty
babeld        2609/tcp                 # BABELd vty
nhrpd         2610/tcp                 # nhrpd vty
pimd          2611/tcp                 # PIMd vty
ldpd          2612/tcp                 # LDPd vty
eigprd        2613/tcp                 # EIGRPd vty
bfdd          2617/tcp                 # bfdd vty
fabricd       2618/tcp                 # fabricd vty

systemctl daemon-reload
systemctl start frr

# Pro Modul gibt es im Pfad /etc/frr eine *.conf Datei, in welcher die Konfiguration der Komponenten hinterlegt werden. Ich nutze aktuell überwiegend zebra

#zebra.conf
hostname monarch-router
password password1
enable password password1
log stdout

# in die Router Config kommt man per Befehl: vtysh, raus per exit
# Hilfe bei Zebra Befehlen über doc: http://docs.frrouting.org/en/latest/zebra.html#zebra-terminal-mode-commands
# Mit Hilfe von conf term kommt man in den config-Bereich (wie bei Cisco)

apt install net-tools

Im Gastsystem (nur ubuntu aktuell) den Output auf ttyS0 umleiten. Dadurch funktioniert dann "virsh console <gastname>" (Strg+]) zum beenden)
sudo systemctl enable serial-getty@ttyS0.service
sudo systemctl start serial-getty@ttyS0.service
oder per ssh auf der Maschine:
ln -s /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@ttyS0.service

Installieren von libvirt-clients (Für qemu Verbindung zu hypervisor + python-libvirt)
apt install libvirt-clients
apt install  pkg-config libvirt-dev

-destroy und undefine und aus xml eine domain definineren:

virsh define monarch.xml zum starten der vm

nested Virtualization prüfen/Hinzufügen: https://docs.fedoraproject.org/en-US/quick-docs/using-nested-virtualization-in-kvm/



ssh-key hinterlegt für authentifizierung ohne passwort: (von ubuntu subsystem aus)
scp "/mnt/c/Users/Markus Betz/.ssh/id_rsa.pub" root@192.168.1.203:/tmp/
cat /tmp/id_rsa.pub >> /root/.ssh/authorized_keys


VM Als Basis-Image konfigurieren:
- apt update && upgrade
- weitere Werkzeuge installieren:  sudo apt install -y traceroute tcpdump nmap

#Key in diesem Repo Verzeichnis /ssh/id_rsa ist dem root user auf dem Monarch im Verzeichnis /root/.ssh/ hinterlegt .
#Er dient der passwortfreien Authentication auf dem KVM Hypervisor und der zugehörige Public Key wurde dort im neu angelegten File #"authorized_keys"  im Verzeichnis /root/.ssh/authorized_keys angelegt
# Hauptgrund ist die Authentifizierun per libvirt.openAuth ,die nur mit Key oder per tcp funktioniert. Für TCP wären allerdings Änderungen
# in /etc/libvirt/libvirtd.conf notwendig, was nicht gewünscht ist, da zu tiefer Eingriff in Hypervisor. --> keine Unabhängigkeit mehr


# monarch weitere IP Configuration, siehe auch Abbildungen in draw io:
network:
  version: 2
  renderer: networkd
  ethernets:
# nethinks:
    ens3f0:
      addresses: [ 192.168.1.203/24 ]
      gateway4: 192.168.1.99
      nameservers:
          addresses:
              - "212.218.0.53"
              - "212.218.0.54"
              - "8.8.8.8"

#hs:
#    ens3:
 #       dhcp4: yes
 #       dhcp6: no
    ens3f1:
      addresses: [ 10.10.100.2/29 ]

    ens3f2:
      addresses: [ 10.10.101.2/29 ]


########## ende ip config

Wenn Internet nicht erreichbar: route del default gw 10.10.101.1
zum löschen dieses default gw für Development.
Danach wieder setzen, da dies der Router sein soll.


---------------------------------
Weitere Ziele:
- SR-IOV oder PCI Passthrough --> Zugriff auf phy Interface direkt zu vm


## Hinweise:
- Mini.iso funktioniert nicht, bzw. nur über Umwege --> https://askubuntu.com/questions/1093411/how-to-use-ubuntu-minimal-images-with-kvm
- Wird in KVM ein Gast via *.xml Datei erstellt, muss diese erst mit define definiert werden (nicht create!) --> Der Befehl ist dann "virsh define monarch.xml"
- Backup der VM die lokal läuft auf qnap NAS H:\Masterarbeit
- Der Ubuntu-Kernel 18.04 enthält einen Bug der dazu führt, dass nested Virtualization nicht korrekt funktioniert. Dies äußert sich speziell bei kvm darüber, dass mit Hilfe der acpi Tools im Guest nur ein shutdown möglich ist. Ein reboot führt dazu, dass die Guest-VM in den paused State wechselt und hart herunter gefahren werden muss (destroy). Danach ist ein undefine nötig. Wird der Gast dann neu defined, funktioniert dieser wie gewohnt. Um dies zu lösen reicht ein Kernel-Update auf 18.10 aus (kein LTS)
Danach muss der Gast gestartet werden. virsh start monarch, Löschen des Gasts: virsh destroy monarch && virsh undefine monarch
Der Hypervisor bekommt eine interne IP zugewiesen (10.0.0.100/24), welche dann nur über die Management VM eirreichbar sein soll.
Zum ziehen von Updates muss dem Hypervisor an interface ens33 eine IP gegebene werden z.B.:
ens33:
  dhcp4: n
  dhcp6: no
  addresses:
    - 192.168.1.202/24
  gateway4: 192.168.1.99
  nameservers:
    addresses:
      - 212.218.0.53
      - 212.218.0.54


- Für HS Präsentationszwecke wurde NAT bzw vmnet8 von VMware so angepasst, dass eine 192.168.1.0/24 Adresse vergeben wird. GW ist 192.168.1.99. Also an der HS die ersten beiden NICs des Hypervisors mit vmnet8 verbinden.  Dies ist so, damit ich nicht ständig die ip config der Adapter ändern muss--> siehe dazu https://www.assono.de/blog/change-nat-vmnet8-subnet-in-vmware-player
 Damit das obige nicht zu Problemen im Nethinks Netz führt, sollte der Adapter vmnet87, vmnet8 deaktiviert werden

Adapter8 muss in Windows mit 192.168.1.99 versehen werden. Aktuell kein Gateway
Die Dateien wie in oben genannter Anleitung müssen angepasst werden C:\ProgramData\VMware



Adapter Config bei Nethinks:
1: Bridged
2: Bridged
3: Host-Only
4: Bridged

LAN aktiviert
vmnet7,8 deaktiviert

Adapter Config an HS:
1: vmnet8
2: vmnet8
3: Host-Only
4: Bridged

WLAN aktiviert
vmnet7,8 aktiviert

Passthrough Devices müssen vom Typ VMXNET3 sein. Dazu muss eine NIC hinzugefügt werden --> Shutdown --> *.vmx anpassen --> reboot
Außerdem: File /etc/default/grub muss angepasst werden zu:
GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on pcie_acs_override=downstream"

über lspci / lspci -n lann man die PCI id und die Hersteller ID herausfinden.
Hier wird für das traditionelle pci Passthrough der pci-stub trieber verwendet. Dazu muss dies geladen werden mit "modprobe pci_stub"



cloud-init image erzeugen:
genisoimage -output config.iso -volid cidata -joliet -rock user-data meta-data network-config



Netzwerk für VMware Internetzugriff:
VMNet8 muss hierfür angepasst werden
Außerdem der Hex Wert auf die DHCP Adresse aus Dateien C:\ProgramData\VMware
in diesem Pfad: Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\VMnetDHCP\Parameters\VirtualEthernetSegments\8
#######################################################################
# monarch Schnittstelle für Verbindung auf KVM-Hypervisor
#######################################################################

# Auf monarch isntallieren:
# apt update
# apt install sshfs
# pip3 install python3-pip
# pip3 install paramiko
# pip3 install sshtunnel
# pip3 install pyfiglet
# pip3 install click
# pip3 install setuptools
# pip3 install libvirt-python




Auf Monarch auch installiert:
https://ubuntu.pkgs.org/16.04/ubuntu-main-i386/cloud-guest-utils_0.27-0ubuntu24_all.deb.html
https://packages.ubuntu.com/xenial/genisoimage


Möglichkeit bei einem PCI Device die pci Adresse herauszufinden und an VM anzuhängen.
Sehr gute Beschreibung:
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_host_configuration_and_guest_installation_guide/chap-virtualization_host_configuration_and_guest_installation_guide-pci_device_config


<network>
  <name>nettest</name>
  <forward mode='hostdev' managed='yes'>
    <pf dev='ens40'/>
  </forward>
</network>


Anforderungen bei Linux Maschinen, die Autodeployt werden:
- müssen cloud-init unterstützen! (python 3 ist eine Abhängigkeit davon)

Bei Linux Anpassen der IPs ist eine Schweirigkeit folgender Bug gewesen:
https://bugs.launchpad.net/netplan/+bug/1768827


Ryu wird als OVS Controller für Openflow eingesetzt.
Dazu wird ryu am monarch isntalliert:
apt install gcc python-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev zlib1g-dev
Und der Beispielswitch eingesetzet: https://github.com/osrg/ryu/blob/master/ryu/app/simple_switch_15.py


######################################################################
#                                Installation OVS
######################################################################

Zur Installation von OVS auf dem KVM Hypervisor werden folgende Befehle ausgeführt:

Hinweis wie VNFs die an OVS angebunden werden sollen konfiguriert werden: https://docs.openvswitch.org/en/latest/howto/libvirt/

apt install openvswitch-switch
apt install uml-utilities

# ifupdown sh configurieren: https://docs.openvswitch.org/en/latest/howto/kvm/
# anderer weg ist mit netword-dispatcher: https://netplan.io/faq#how-to-go-back-to-ifupdown

#setup sfc bridge:
ovs-vsctl add-br sfcbr0

# füge Port hinzu:
ovs-vsctl add-port sfcbr0 ens40

# befehl für ovs config:  ovs-ofctl show sfcbr0
# befehl komplette ovs config: ovs-vsctl show

# teste openflow connection:
<!-- ovs-ofctl add-flow sfcbr0 in_port=1,priority=5,dl_dst=52:54:00:00:00:05,actions=2

ovs-ofctl add-flow sfcbr0 in_port=2,priority=5,dl_dst=52:54:00:00:00:06,actions=4
ovs-ofctl add-flow sfcbr0 in_port=4,priority=5,dl_dst=52:54:00:00:00:05,actions=2

ovs-ofctl add-flow sfcbr0 priority=5,in_port=1,dl_dst=01:00:00:00:00:00/01:00:00:00:00:00,actions=2

ovs-ofctl add-flow sfcbr0 priority=5,in_port=2,dl_dst=01:00:00:00:00:00/01:00:00:00:00:00,actions=4
ovs-ofctl add-flow sfcbr0 priority=5,in_port=4,dl_dst=01:00:00:00:00:00/01:00:00:00:00:00,actions=2 -->





# add ovsbr0 to KVM # datei vorher dorthin angelegt/kopiert
virsh net-define /datastore/iso/sfcbr0.xml
virsh net-start sfcbr0
virsh net-autostart sfcbr0


# Aktuell werden alle Flows verloren bei KVM Hypervisor restart
# alle flows anzeigen:
ovs-ofctl dump-flows sfcbr0



# Falls keine qemu connection herstellbar sein sollte:
ufw allow proto tcp from any to any port 16509



# Hinweis: Hostname in ubuntu ändern: Dateien /etc/hostname und /etc/hosts anpassen

# Hinweis: super Anleitung für PCI Passthrough: http://www.linux-kvm.org/page/How_to_assign_devices_with_VT-d_in_KVM
# https://gist.github.com/mdPlusPlus/031ec2dac2295c9aaf1fc0b0e808e21a



######################################################################
#                                ALT
######################################################################
- 212.218.100.103

Das Default-Gateway ist 212.218.100.1 (pe1-nbg1)- keine Firewall


pmanager1-nbg1:
name: nethinks
username: nethinks
pw:fde...



MANO Komponente:
Entscheidung:
Mögliche Implementierungen:
OSM
openBaton --> Vorteil gebaut zum Verwalten von mehreren VIM Instanzen
open-o


MANO liegt nicht im Fokus.

Was passieren soll ist, eine virtuelle Netzwerkumgebung nach nethinks Anforderungen zu bauen.

Hinweise:
- Zuerst wir eine manuelle Infrastruktur installiert und geschaut, wie dies funktionieren kann --> Snapshots!
  - Ubuntu dient als KVM Hypervisor. Ich nehme erst die Cloud-Version. Später vielleicht versuchen minimale kernel Variante zu nutzen und nur die relevanten Pakete installieren.
- ist das erledit, wird automatisiert und eventuell Controller für die Config eingesetzt. Vielleicht auch eigene Konfig parsen.
