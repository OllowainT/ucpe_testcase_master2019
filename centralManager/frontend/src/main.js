import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Ionic from '@ionic/vue/'
import '@ionic/core/css/ionic.bundle.css'
import { addIcons } from "ionicons";
import { star, trash, create } from "ionicons/icons";
import VueCodemirror from 'vue-codemirror'
Vue.use(VueCodemirror, {options: {theme: 'base16-dark', tabSize: 4,
        styleActiveLine: true,
        lineNumbers: true,
        line: true,
        mode: 'text/javascript',
        lineWrapping: true,
        theme: 'default'}})

Vue.use(Ionic);

import api from './util/api';
Vue.use(api);

import i18n from './util/i18n';
Vue.use(i18n);

addIcons({
  "ios-star": star.ios,
  "md-star": star.md,
  "ios-trash": trash.ios,
  "md-trash": trash.md,
  "ios-create": create.ios,
  "md-create": create.md
});

Vue.config.ignoredElements = [/^ion-/]
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
