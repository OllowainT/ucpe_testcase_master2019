import Vue from 'vue'
import {IonicVueRouter} from '@ionic/vue'
  
import home from './pages/home.vue'
import sidebar from './components/sidebar.vue'
import settings from './pages/settings.vue'
  
Vue.use(IonicVueRouter)

export default new IonicVueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/sidebar',
      name: 'sidebar',
      component: sidebar
    },
    {
      path: '/settings',
      name: 'settings',
      component: settings
    }
  ]
})