import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);

const endpoint = 'http://127.0.0.1:7000/api/';
const ucpe_id1 = 'http://192.168.1.203:5000/';

var postHeader = {
  method: "POST",
  headers: {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "POST",
  "Access-Control-Max-Age": 86400,
  "Access-Control-Allow-Headers": "Content-Type, application/json",
  'Access-Control-Allow-Credentials': 'true',
  'Content-Type': 'application/json'}
}


const api = function(path, option){
  return new Promise(function(resolve, reject){
    Vue.axios.get(endpoint + path).then(function(response){
      resolve(response.data);
    });
  });
}

const ucpe_id1_api = function(path, body){
  return new Promise(function(resolve, reject){
    Vue.axios.post(ucpe_id1 + path, body, postHeader).then(function(response){
      resolve(response.data);
    });
  });
}

export default {
  install(Vue, options){
    Vue.prototype.$api = api;
    Vue.prototype.$ucpe_id1_api = ucpe_id1_api;
  }
}
