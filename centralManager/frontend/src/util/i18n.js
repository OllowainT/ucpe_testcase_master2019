import i18next from 'i18next';
import de from '../translation/de.js';

export default {
  install(Vue, options){  
    i18next.init({
      lng: 'de',
      resources: {
        de: {
          translation: de
        }
      }
    });

    Vue.prototype.$t = function(keys, options) {
      return i18next.t(keys, options);
    }
  }
}