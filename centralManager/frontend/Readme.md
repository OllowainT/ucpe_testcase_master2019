# frontend
This is the setup for the frontend of the Materthesis.
There will be just some basic functions, to show the most important functions

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


#Ein Hinweis zum Testaufbaiu: In der Datei package.json im Frontend lässt sich die IP definieren, die neben dem localhost genutzt wird! Bei mir daheim z.B. 192.168.178.99
