yaml = require('js-yaml');
fs   = require('fs');
var path = require('path');


module.exports = {load_config_by_id};

var manager_config = require('./manager_config');

function load_config_by_id(id){
  console.log(manager_config.uCPE_configfile_path)
  cfg_path = path.join(__dirname, manager_config.uCPE_configfile_path)
  var cfg = yaml.safeLoadAll(fs.readFileSync(cfg_path, 'utf8'));
  console.log(cfg)
  return cfg
}
