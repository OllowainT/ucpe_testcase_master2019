const express = require('express')

////////////////////////////////////////////////
// Backend Definition
////////////////////////////////////////////////


const app = express();

var bl = require('./business_logic');



app.listen(7000, '0.0.0.0', () => {
  console.log('Example app listening on port 7000!')
});

////////////////////////////////////////////////
// Objects
////////////////////////////////////////////////

var ucpes = [
  {
    "id" : "id1",
    "name" : "First_uCPE",
    "customer" : "Abc GmbH",
    "init_date" : "2019-10-28",
    "state" : "initial",
    "ip" : "10.10.101.3"
  },
  {
    "id" : "id2",
    "name" : "Second_uCPE",
    "customer" : "Def GmbH",
    "init_date" : "2019-10-08",
    "ip" : "36.121.12.3"
  },
  {
    "id" : "id3",
    "name" : "Test_Device",
    "customer" : "Nethinks GmbH",
    "init_date" : "2019-09-14",
    "state" : "configured",
    "ip" : "210.12.43.123"
  }
]

////////////////////////////////////////////////
// Rest API:
////////////////////////////////////////////////

//worktest
app.get('/api', (req, res) => {
  res.send('Hello World!')
});

// get ucpe config by id
app.get('/api/config/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  var id = req.params.id
  var config = bl.load_config_by_id(id)
  res.send(JSON.stringify(config, null, 2))

});

//get all ucpes
app.get('/api/ucpes/', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.send(JSON.stringify(ucpes))
});
